package com.example;

import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.entity.UserInfo;
import com.example.service.UserInfoService;
import com.example.utils.DateUtil;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserInfoServiceTest {
	
    @Autowired
	UserInfoService userInfoService;
    
    @Test
    public void findUserInfoList() {
		List<UserInfo> users = userInfoService.getUserInfoList();
		System.out.println(users.size());
	}
    
    @Test
    public void deleteById() {
    	//userInfoService.deleteByName("111");
    	userInfoService.deleteUserById(5);
    	List<UserInfo> users = userInfoService.getUserInfoList();
		System.out.println(users.size());
	}
    
    @Test
    public void findUserInfoById() {
        UserInfo user = userInfoService.getOneByJpql(2);
		System.out.println("11:"+user.toString());
	}
    
    
    @Test
    public void save() {
    	UserInfo user = new UserInfo();
    	user.setId(Integer.parseInt(DateUtil.getNowTimeStamp()));
    	user.setPassword("d3c59d25033dbf980d29554025c23a75");
    	user.setSalt("8d78869f470951332959580424d4bf4f");
    	user.setCreateTime(new Date());
    	user.setUpdateTime(new Date());
    	user.setState(new Byte("0"));
    	user.setUsername("666");
    	user.setName("666");
    	userInfoService.save(user);
		System.out.println("11:"+user.toString());
	}
    
    

}
