package com.example.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.apache.ibatis.annotations.Param;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.example.entity.UserInfo;

//public interface UserInfoRepository extends JpaRepository<UserInfo, Long> {
//自定义sql
//public interface UserInfoRepository extends JpaRepository<UserInfo, Long>,JpaSpecificationExecutor<UserInfo>,Serializable{
public interface UserInfoRepository extends CrudRepository<UserInfo, Long> {

	@Transactional
	@Modifying
	@Query("delete from UserInfo where username = ?1")
	void deleteByUserName(String username);
	
	@Modifying
	@Transactional
	@Query("delete from UserInfo u where u.id=:id")
	public int deleteUserById(@Param("id") Integer id);
	
	@Query(value = "select u from UserInfo u where u.id = ?1")
	public UserInfo getOneByJpql(Integer id);
	
    public UserInfo findByUsername(String username);
    
    public UserInfo findByUsernameAndPassword(String username, String password);
    
//    Page<User> findALL(Pageable pageable);
    
    public List<UserInfo> findByUsernameIn(List<String> UsernameList);

}
