package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootShiroApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootShiroApplication.class, args);
        String strConsole="\n" +
        		"    *           *      \n" +
    		    " *     *     *     *	\n" +                                  
    		    " *        *        *   \n" +
    		    "  *   ZlyFlycat   *	\n" +
    		    "    *           *   	\n" +
    		    "       *     *      	\n" +
    		    "          *    		\n"+
    		    "  ლ(´ڡ`ლ)ﾞ  ZlyFlycatAdmin started successfully!!!  (♥◠‿◠)ﾉﾞ  \n";
        System.out.println(strConsole);
	}

}
