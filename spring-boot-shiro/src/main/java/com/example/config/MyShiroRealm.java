package com.example.config;

import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.util.ByteSource;
import org.springframework.beans.factory.annotation.Autowired;

import com.example.entity.SysPermission;
import com.example.entity.SysRole;
import com.example.entity.UserInfo;
import com.example.service.UserInfoService;

/**
 * (实现AuthorizingRealm接口用户用户认证)
 */
public class MyShiroRealm extends AuthorizingRealm {
    
	/*角色权限和对应权限添加*/
    @Autowired
    private UserInfoService userInfoService;
	
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
        SimpleAuthorizationInfo authorizationInfo = new SimpleAuthorizationInfo();
        //获取登录用户名
        String name= (String) principals.getPrimaryPrincipal();
        //查询用户信息
        UserInfo userInfo  = (UserInfo)principals.getPrimaryPrincipal();
        System.out.println("权限配置:  ---->获取登录用户名："+name+" 获取角色信息："+userInfo.getRoleList());
               
        for(SysRole role:userInfo.getRoleList()){
            //添加角色
        	authorizationInfo.addRole(role.getRole());
            for(SysPermission p:role.getPermissions()){
            	//添加权限
                authorizationInfo.addStringPermission(p.getPermission());
            }
        }
        return authorizationInfo;
    }

    /*主要是用来进行身份认证的，也就是说验证用户输入的账号和密码是否正确。*/
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token)
            throws AuthenticationException {
        //获取用户的输入的账号.
        String username = (String)token.getPrincipal();
        //通过username从数据库中查找 User对象，如果找到，没找到.
        //实际项目中，这里可以根据实际情况做缓存，如果不做，Shiro自己也是有时间间隔机制，2分钟内不会重复执行该方法
        UserInfo userInfo = userInfoService.findByUserName(username);
        System.out.println("用户认证： ---->获取登录用户名："+username+" ---->"+userInfo.toString());
        if(userInfo == null){
            return null;
        }
        //这里验证authenticationToken和simpleAuthenticationInfo的信息
        SimpleAuthenticationInfo authenticationInfo = new SimpleAuthenticationInfo(
                userInfo, //用户名
                userInfo.getPassword(), //密码
                ByteSource.Util.bytes(userInfo.getCredentialsSalt()),//salt=username+salt
                getName()  //realm name
        );
        return authenticationInfo;
    }

}