package com.example.service.impl;


import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.example.entity.UserInfo;
import com.example.repository.UserInfoRepository;
import com.example.service.UserInfoService;

@Service
public class UserInfoServiceImpl implements UserInfoService{

    @Autowired
    private UserInfoRepository userInfoRepository;

	@Override
	@Transactional(propagation =Propagation.SUPPORTS)
	public List<UserInfo> getUserInfoList() {
		return (List<UserInfo>) userInfoRepository.findAll();
	}
	
	@Override
	@Transactional(propagation =Propagation.SUPPORTS)
	public UserInfo getOneByJpql(Integer userId) {
		return userInfoRepository.getOneByJpql(userId);
	}

	@Override
	@Transactional(propagation =Propagation.SUPPORTS)
	public UserInfo findByUserName(String username) {
		System.out.println("UserInfoServiceImpl.findByUsername()");
        return userInfoRepository.findByUsername(username);
	}
	
	@Override
	public UserInfo findByUserNameAndPassword(String userName, String password) {
		System.out.println("UserInfoServiceImpl.findByUserNameAndPassword()");
		return userInfoRepository.findByUsernameAndPassword(userName, password);
	}

	@Override
	@Transactional(propagation =Propagation.REQUIRED)
	public void save(UserInfo userInfo) {
		userInfoRepository.save(userInfo);
	}

	@Override
	@Transactional(propagation =Propagation.REQUIRED)
	public void edit(UserInfo userInfo) {
		userInfoRepository.save(userInfo);
	}

	@Override
	@Transactional(propagation =Propagation.REQUIRED)
	public Integer deleteUserById(Integer id) {
		return userInfoRepository.deleteUserById(id);
	}

	@Override
	@Transactional(propagation =Propagation.REQUIRED)
	public void deleteByName(String username) {
		userInfoRepository.deleteByUserName(username);
	}
	
}


