package com.example.service;

import java.util.List;

import com.example.entity.UserInfo;

public interface UserInfoService {
	
    public List<UserInfo> getUserInfoList();
    
    public UserInfo getOneByJpql(Integer id);

    public UserInfo findByUserName(String username);
    
    public UserInfo findByUserNameAndPassword(String UserName,String passWord);
    
    public void save(UserInfo UserInfo);

    public void edit(UserInfo UserInfo);

    public Integer deleteUserById(Integer id);

//    public Page<UserInfo> findALL(Pageable pageable);
    
    public void deleteByName(String UserInfoName);
}
