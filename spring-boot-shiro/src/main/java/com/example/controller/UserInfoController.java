package com.example.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.entity.UserInfo;
import com.example.service.UserInfoService;
import com.example.utils.DateUtil;


@Controller
@RequestMapping("/userInfo")
public class UserInfoController {

    @Autowired
	UserInfoService userInfoService;
	
    @RequestMapping({"/","/index"})
	public String index1w() {
		return "user/userList";
	}
    
    /**
     * (用户查询)<br/>
     */
    @RequestMapping("/userList")
    //@RequiresPermissions("userInfo:view")//权限管理;
	public String userInfo(Model model) {
		List<UserInfo> users = userInfoService.getUserInfoList();
		System.out.println("users:"+users.toString());
		model.addAttribute("users", users);
		return "user/userList";
	}
    
    /**
     * (用户添加)<br/>
     */
    @RequestMapping("/toAdd")
    //@RequiresPermissions("userInfo:add")//权限管理;
	public String toAdd() {
		return "user/userInfoAdd";
	}

	@RequestMapping("/add")
	//@RequiresPermissions("userInfo:add")//权限管理;
	public String add(UserInfo user) {
		user.setId(Integer.parseInt(DateUtil.getNowTimeStamp()));
		user.setPassword("d3c59d25033dbf980d29554025c23a75");
		user.setSalt("8d78869f470951332959580424d4bf4f");
		user.setState(new Byte("0"));
		user.setCreateTime(new Date());
		user.setUpdateTime(new Date());
		System.out.println("add:"+user.toString());
		userInfoService.save(user);
		return "redirect:userList";
	}

	@RequestMapping("/toEdit")
	public String toEdit(Model model, Integer id) {
		try {
			UserInfo user = userInfoService.getOneByJpql(id);
			model.addAttribute("user", user);
		} catch (Exception e) {
			System.err.println(e);
		}
		return "user/userInfoEdit";
	}

	@RequestMapping("/edit")
	public String edit(UserInfo user) {
		user.setUpdateTime(new Date());
		userInfoService.edit(user);
		return "redirect:userList";
	}

	@RequestMapping("/delete")
	public String deleteUserById(Integer id) {
		int result =userInfoService.deleteUserById(id);
		System.out.println("删除"+id+" ===>结果："+result);
		return "redirect:userList";
	}
	
}
