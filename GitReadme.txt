简易的命令行入门教程:
Git 全局设置:

git config --global user.name "Flycatzly"
git config --global user.email "2286704940@qq.com"
创建 git 仓库:

mkdir sping-boot-demo
cd sping-boot-demo
git init
touch README.md
git add README.md
git commit -m "first commit"
git remote add origin https://gitee.com/flycatzly/sping-boot-demo.git
git push -u origin master


已有项目?

cd existing_git_repo
git remote add origin https://gitee.com/flycatzly/sping-boot-demo.git
git push -u origin master



项目克隆到本地
git clone https://gitee.com/flycatzly/sping-boot-demo.git

git add --all
git commit -m "第一次提交"
git push -u origin master



//（查看本地分支文件信息，确保更新时不产生冲突）
git status

//（若文件有修改，可以还原到最初状态; 若文件需要更新到服务器上，应该先merge到服务器，再更新到本地）
git checkout -- [file name] 

//（查看当前分支情况）
git branch

//(若分支为本地分支，则需切换到服务器的远程分支)
git checkout [remote branch]

git pull



码云的账号和密码：
2286704940@qq.com/qaz0820

解决方法二：Windows凭据设置

控制面板->用户账户->凭据管理器->Windows凭据


普通凭据下
git:https://github.com/或git:https://gitee.com/

删除操作：
点击删除->
git push origin master->
重新弹出输入账号密码界面


修改操作：点击修改->填写正确的账号密码->保存