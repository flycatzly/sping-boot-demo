package com.example;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.entity.User;
import com.example.repository.UserRepository;
import com.example.utils.DateUtil;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserRepositoryTest {
	
	@Autowired
	private UserRepository userRepository;
	
	@Test
	@Ignore
	public void testBaseQuery() throws Exception {
		System.out.println(userRepository.count());
		System.out.println(userRepository.findAll());
	}
	
	@Test
	@Ignore
	public void findByUserName() throws Exception {
		User user =userRepository.findByUserName("zly");
		System.out.println(user);
	}
	
	@Test
	@Ignore
	public void findByUserNameAndPassword() throws Exception {
		User user =userRepository.findByUserNameAndPassword("999","999");
		System.out.println(user);
	}
	
	/**
	 * 测试In查询
	 */
	@Test
	@Ignore
	public void findByUserNameInTest(){
		List<String> list = new ArrayList<String>();
		list.add("99");
		list.add("88");
		list.add("77");
		List<User> result =userRepository.findByUserNameIn(list);
		System.out.println(result.size()+"\t"+result);
		Assert.assertNotEquals(0, result.size());
		
	}
	
	@Test
	@Ignore
	public void insert() throws Exception {
		User user=new User();
		//user.setId(11);
		user.setUserName(DateUtil.getNowTimeStamp()+"_Name");
		user.setPassword(DateUtil.getNowTimeStamp()+"_password");
		user.setAge(99);
		userRepository.save(user);
		System.out.println(userRepository.count());
	}
	
	@Test
	@Ignore
	public void update() throws Exception {
		User user=new User();
		user.setId(2);
		user.setUserName("zly1");
		user.setPassword("1111");
		user.setAge(111);
		userRepository.save(user);
		System.out.println(userRepository.count());
	}
	
	@Test
	@Ignore
	public void delete() throws Exception {
		User user=new User();
		user.setId(3);
		userRepository.delete(user);
		System.out.println(userRepository.count());
	}
	
	/**
	 * 自定义sql byID 删除
	 * @throws Exception
	 */
	@Test
	@Ignore
	public void deleteById() throws Exception {
		userRepository.deleteById(4);
		userRepository.deleteById(5);
		System.out.println(userRepository.count());
	}
	
	/**
	 * 自定义sql by name 删除
	 * @throws Exception
	 */
	@Test
	@Ignore
	public void deleteByName() throws Exception {
		userRepository.deleteByUserName("zly1");
		System.out.println(userRepository.count());
	}
	
}
