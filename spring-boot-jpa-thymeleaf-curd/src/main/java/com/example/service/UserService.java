package com.example.service;


import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.example.entity.User;

public interface UserService {

    public List<User> getUserList();

    public User findUserById(long id);
    
    public User findByUserName(String userName);
    
    public void save(User user);

    public void edit(User user);

    public void deleteById(Long id);
    
    public User findByUserNameAndPassword(String userName,String passWord);

//    public Page<User> findALL(Pageable pageable);
    
    public void deleteByName(String userName);
    
}
