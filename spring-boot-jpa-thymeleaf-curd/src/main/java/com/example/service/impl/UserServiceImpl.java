package com.example.service.impl;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.example.entity.User;
import com.example.repository.UserRepository;
import com.example.service.UserService;

@Service
public class UserServiceImpl implements UserService{

    @Autowired
    private UserRepository userRepository;

	@Override
	@Transactional(propagation =Propagation.SUPPORTS)
	public List<User> getUserList() {
		return userRepository.findAll();
	}

	@Override
	@Transactional(propagation =Propagation.SUPPORTS)
	public User findUserById(long id) {
		return userRepository.findById(id);
	}

	@Override
	@Transactional(propagation =Propagation.REQUIRED)
	public void save(User user) {
		userRepository.save(user);
	}

	@Override
	@Transactional(propagation =Propagation.REQUIRED)
	public void edit(User user) {
		userRepository.save(user);
	}

	@Override
	@Transactional(propagation =Propagation.REQUIRED)
	public void deleteById(Long id) {
		userRepository.deleteById(id);
	}

	@Override
	@Transactional(propagation =Propagation.SUPPORTS)
	public User findByUserName(String userName) {
		User user = new User();
        user = userRepository.findByUserName(userName);
        return user;
	}

	@Override
	@Transactional(propagation =Propagation.SUPPORTS)
	public User findByUserNameAndPassword(String userName, String passWord) {
		User user = new User();
        user = userRepository.findByUserNameAndPassword(userName, passWord);
        return user;
	}

	@Override
	@Transactional(propagation =Propagation.REQUIRED)
	public void deleteByName(String userName) {
		userRepository.deleteByUserName(userName);
	}

//	@Override
//	@Transactional(propagation =Propagation.SUPPORTS)
//	public Page<User> findALL(Pageable pageable) {
//		 return userRepository.findAll(pageable);
//	}

}


