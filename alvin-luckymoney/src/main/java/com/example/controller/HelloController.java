package com.example.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.config.LimitConfig;

@RestController
public class HelloController {

	@Autowired
	private LimitConfig limitConfig;

	@GetMapping({ "/hello", "hi" })
	public String hello() {
		return "最大金额:" + limitConfig.getMinMoney() + ",最小金额：" + limitConfig.getMinMoney() + ",说明："
				+ limitConfig.getDescription();
	}

	// http://localhost:8081/luckymoney/say/520
	@GetMapping("/say/{id}")
	public String say(@PathVariable("id") Integer id) {
		return "id:" + id;
	}

	// http://localhost:8081/luckymoney/say1?id=520 默认值必传
	@GetMapping("/say1")
	public String say1(@RequestParam("id") Integer id) {
		return "传入的参数：" + id;
	}
	
	// http://localhost:8081/luckymoney/say2 设置值非必传
	@GetMapping("/say2")
	public String say2(@RequestParam(value = "id", required = false, defaultValue = "0") Integer id) {
		return "传入的参数：" + id;
	}

}
