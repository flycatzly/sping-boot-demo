package com.example.controller;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.entity.Luckymoney;
import com.example.repository.LuckymoneyRepository;
import com.example.service.LunckymoneyService;
@RestController
public class LuckymoneyController {

	@Autowired
	private LuckymoneyRepository repository;
	
	@Autowired
	private LunckymoneyService service;

	/**
	 * 全部红包
	 */
	@GetMapping("/luckymoneys")
	public List<Luckymoney> list() {
		return repository.findAll();
	}

	/**
	 * 发红包
	 */
	@PostMapping("/luckymoneys")
	public Luckymoney create(@RequestParam("producer") String producer,@RequestParam("money") BigDecimal money) {
		Luckymoney luckymoney= new Luckymoney();
		luckymoney.setProducer(producer);
		luckymoney.setMoney(money);
		return repository.save(luckymoney);
	}
	
	/**
	 * 通过id查红包
	 */
	@GetMapping("/luckymoneys/{id}")
	public Luckymoney findbyId(@PathVariable("id") Integer id) {
		return repository.findById(id).orElse(null);
	}

	/**
	 * 更新红包
	 */
	@PutMapping("/luckymoneys/{id}")
	public Luckymoney update(@PathVariable("id") Integer id,@RequestParam("consumer") String consumer) {
		Optional<Luckymoney> optional=repository.findById(id);
		if(optional.isPresent()) {
			Luckymoney luckymoney= optional.get();
			luckymoney.setConsumer(consumer);
			return repository.save(luckymoney);
		}
		return null;
	}
	
	/**
	 * 发红包
	 */
	@PostMapping("/luckymoneys/two")
	public void createTwo() {
		service.createTwo();
	}

}
