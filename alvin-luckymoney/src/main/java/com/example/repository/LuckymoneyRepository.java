package com.example.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.example.entity.Luckymoney;

//public interface LuckymoneyRepository extends JpaRepository<OrderDetail, String> {
public interface LuckymoneyRepository extends JpaRepository<Luckymoney, Integer>,JpaSpecificationExecutor<Luckymoney>,Serializable{

	
}
