package com.example.config;

import java.math.BigDecimal;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Data;

@Component
@ConfigurationProperties(prefix = "limit")
@Data
public class LimitConfig {

	private BigDecimal minMoney;
	private BigDecimal maxMoney;
	private String description;
	
}
