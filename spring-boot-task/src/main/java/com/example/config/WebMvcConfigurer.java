package com.example.config;


import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import com.example.contoller.interceptor.OneInterceptor;
import com.example.contoller.interceptor.TwoInterceptor;
/**
 * 拦截器
 * 1.使用注解@Configuration 配置拦截器
 * 2.继承WebMvcConfigurerAdapter
 * 3.重写addInterceptors添加需要的拦截器地址
 * @author Administrator
 *
 */
@Configuration
public class WebMvcConfigurer extends WebMvcConfigurerAdapter {

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		/**
		 * 拦截器按照顺序执行
		 */
		System.out.println("进入拦截器 addInterceptiors方法中...");
		//拦截全部 Contoller
//		registry.addInterceptor(new OneInterceptor()).addPathPatterns("/*/**");
		registry.addInterceptor(new OneInterceptor()).addPathPatterns("/one/**");
		registry.addInterceptor(new TwoInterceptor()).addPathPatterns("/two/**").addPathPatterns("/one/**");
		
		super.addInterceptors(registry);
	}

}
