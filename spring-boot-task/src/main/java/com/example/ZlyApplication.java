package com.example;

import tk.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
//扫描mapper包路径
@MapperScan(basePackages="com.example.mapper")	
//扫描所需要的包，包含自用的工具类包
@ComponentScan(basePackages= {"com.example","org.n3r.idworker"})	
//开启定时任务
@EnableScheduling
//开启异步调用方法
@EnableAsync
public class ZlyApplication {

	public static void main(String[] args) {
		SpringApplication.run(ZlyApplication.class, args);
        String strConsole="\n" +
        		"    *           *      \n" +
    		    " *     *     *     *	\n" +                                  
    		    " *        *        *   \n" +
    		    "  *   ZlyFlycat   *	\n" +
    		    "    *           *   	\n" +
    		    "       *     *      	\n" +
    		    "          *    		\n"+
    		    "  ლ(´ڡ`ლ)ﾞ  ZlyFlycatAdmin started successfully!!!  (♥◠‿◠)ﾉﾞ  \n";
        System.out.println(strConsole);
	}
}
