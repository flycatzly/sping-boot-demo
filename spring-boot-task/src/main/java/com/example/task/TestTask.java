package com.example.task;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 *  定时任务 test
 * @author Administrator
 *
 */
@Component
public class TestTask {
	
	private static final SimpleDateFormat dataFormat =new SimpleDateFormat("HH:mm:ss.SSS"); 
	
	//定义每过6秒执行任务
//	@Scheduled(fixedRate=6000)
	//每到6秒到36时开始执行
//	@Scheduled(cron="6-36 * * * * ?")		//cron.qqe2.com  
	public void reportCurrentTime() {
		System.out.println(dataFormat.format(new Date())+" 正在执行定时任务.");
	}

}
