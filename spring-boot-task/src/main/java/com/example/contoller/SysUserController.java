package com.example.contoller;

import java.util.Date;
import java.util.List;

import org.aspectj.util.FileUtil;
import org.n3r.idworker.Sid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.thymeleaf.util.StringUtils;

import com.example.pojo.IZlyJSONResult;
import com.example.pojo.SysUser;
import com.example.service.UserService;
import com.example.utils.DateUtil;

@RestController
@RequestMapping("/sysUser")
public class SysUserController {

	@Autowired
	private Sid sid;

	@Autowired
	private UserService userService;

	@RequestMapping("/saveUser")
	public IZlyJSONResult saveUser() throws Exception {
		String userId = sid.nextShort();
		SysUser user = new SysUser();
		user.setId(userId);
		user.setUsername("FlycatZly" + DateUtil.getNowTimeStamp());
		user.setNickname("FlycatZly" + DateUtil.getNowTimeStamp());
		user.setPassword("621014");
		user.setAge(18);
		user.setIsDelete(0);
		user.setRegistTime(new Date());
		userService.saveUser(user);
		return IZlyJSONResult.ok("保存成功");
	}

	@RequestMapping("/updateUser")
	public IZlyJSONResult updateUser() {
		SysUser user = new SysUser();
		user.setId("test001");
		user.setUsername("test001-updated" + sid.nextShort());
		user.setNickname("test001-updated" + sid.nextShort());
		user.setPassword("test001-updated");
		user.setAge(20);
		user.setJob(111);
		user.setIsDelete(0);
		user.setRegistTime(new Date());
		userService.updateUser(user);
		return IZlyJSONResult.ok("修改成功");
	}

	@RequestMapping("/deleteUser")
	public IZlyJSONResult deleteUser(String userId) {
		if (StringUtils.isEmpty(userId)) {
			return IZlyJSONResult.errorMsg("用户名ID不能为空！！！");
		} else {
			userService.deleteUser(userId);
			return IZlyJSONResult.ok("删除成功");
		}
	}

	@RequestMapping("/queryUserById")
	public IZlyJSONResult queryUserById(String userId) {
		if (StringUtils.isEmpty(userId)) {
			return IZlyJSONResult.errorMsg("用户名ID不能为空！！！");
		} else {
			SysUser user = userService.queryUserById(userId);
			if (user == null || "".equals(user)) {
				return IZlyJSONResult.errorMsg("该用户名ID没有数据！！！");
			} else {
				return IZlyJSONResult.ok(user);
			}
		}
	}

	@RequestMapping("/queryUserByIdCustom")
	public IZlyJSONResult queryUserByIdCustom(String userId) {
		if (StringUtils.isEmpty(userId)) {
			return IZlyJSONResult.errorMsg("用户名ID不能为空！！！");
		} else {
			SysUser user = userService.queryUserByIdCustom(userId);
			if (user == null || "".equals(user)) {
				return IZlyJSONResult.errorMsg("该用户名ID没有数据！！！");
			} else {
				return IZlyJSONResult.ok(user);
			}
		}
	}

	@RequestMapping("/queryUserListPaged")
	public IZlyJSONResult queryUserListPaged(Integer page, Integer pageSize) {
		if (page == null) {
			page = 1;
		}
		if (pageSize == null) {
			pageSize = 10;
		}
		SysUser user = new SysUser();
		List<SysUser> userList = userService.queryUserListPaged(user, page, pageSize);
		return IZlyJSONResult.ok(userList);
	}

	@RequestMapping("/saveUserTransactional")
	public IZlyJSONResult saveUserTransactional() {

		String userId = sid.nextShort();

		SysUser user = new SysUser();
		user.setId(userId);
		user.setUsername("lee_" + DateUtil.getNowTimeStamp());
		user.setNickname("lee_" + DateUtil.getNowTimeStamp());
		user.setPassword("abc123");
		user.setIsDelete(0);
		user.setRegistTime(new Date());
		userService.saveUserTransactional(user);
		return IZlyJSONResult.ok("事物执行成功");
	}

}
