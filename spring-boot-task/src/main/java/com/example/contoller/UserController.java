package com.example.contoller;

import java.util.Date;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.pojo.IZlyJSONResult;
import com.example.pojo.User;

@RestController
@RequestMapping("/user")
public class UserController {
	
    @RequestMapping("/getUser")
    public User getUser() {
    	User user = new User();
    	user.setName("FlycatZly");
    	user.setPassword("621014");
    	user.setAge(18);
    	user.setBirthday(new Date());
    	user.setDesc(null);
        return user;
    }
    
    @RequestMapping("/getUserJson")
    public IZlyJSONResult getUserJson() {
    	User user = new User();
    	user.setName("FlycatZly");
    	user.setPassword("621014");
    	user.setAge(18);
    	user.setBirthday(new Date());
    	user.setDesc("this is me!!!");
        return IZlyJSONResult.ok(user);
    }
   
}
