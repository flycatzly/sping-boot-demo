package com.example.controller;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.entity.User;
import com.example.service.UserService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;

@Controller
@RequestMapping("/user")
@Api(description="用户管理")
public class UserController {

	@Resource
	UserService userService;

	@RequestMapping("/list")
	public String list(Model model) {
		List<User> users = userService.getUserList();
		model.addAttribute("users", users);
		return "user/list";
	}
	
	@ResponseBody
	@GetMapping("/getUser/{id}")
	@ApiOperation(value="根据ID获取用户信息",notes="根据url的id来获取用户详细信息")
	@ApiImplicitParam(name="id",value="用户id",required=true, dataType="long",paramType="path")
	public User getUserById(@PathVariable("id")Long id) {
		return userService.findUserById(id);
	}
	
	@RequestMapping("/toAdd")
	public String toAdd() {
		return "user/userAdd";
	}

	@RequestMapping("/add")
	public String add(User user) {
		userService.save(user);
		return "redirect:/user/list";
	}

	@RequestMapping("/toEdit")
	public String toEdit(Model model, Long id) {
		User user = userService.findUserById(id);
		model.addAttribute("user", user);
		return "user/userEdit";
	}

	@RequestMapping("/edit")
	public String edit(User user) {
		userService.edit(user);
		return "redirect:/user/list";
	}
   
	@RequestMapping("/delete")
	public String delete(Long id) {
		userService.deleteById(id);
		return "redirect:/user/list";
	}
}
