
package com.example.utils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.Key;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.Random;
import java.util.Scanner;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;

import sun.misc.BASE64Decoder;

public class StringUtil {

	private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	
	//随机数
	private static Random strGen = new Random(); 
	private static Random numGen = new Random(); 
	private static char[] numbersAndLetters = ("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ").toCharArray();
	private static char[] numbers = ("0123456789").toCharArray();
//	public static Scanner input = new Scanner(System.in);

	public static String conversionSpecialCharacters(String string) {
		return string.replaceAll("\\\\", "/");
	}
	
	/**
	 * 非空判断   --字符串不为空时为true
	 * 方法名： isNotEmpty <BR>
	 * @param str
	 * @return boolean <BR>
	 */
	public static boolean isNotEmpty(String str) {
		return !isEmpty(str);
	}
	
	/**
	 * 空判断  --判断字符串为空时true
	 * 方法名： isEmpty <BR>
	 * @param str
	 * @return boolean <BR>
	 */
	public static boolean isEmpty(String str) {
		return null == str || str.length() == 0 || "".equals(str)|| "null".equals(str)
				|| str.matches("\\s*");
	}
	
    /** 
     * 判断对象为空 
     *  
     * @param obj 对象名 
     * @return 是否为空 
     */  
    public static boolean isEmptyObj(Object obj)  
    {  
        if (obj == null)  
        {  
            return true;  
        }  
        if ((obj instanceof List))  
        {  
            return ((List) obj).size() == 0;  
        }  
        if ((obj instanceof String))  
        {  
            return ((String) obj).trim().equals("");  
        }  
        return false;  
    }  
      
    /** 
     * 判断对象不为空 
     *  
     * @param obj 
     *            对象名 
     * @return 是否不为空 
     */  
    public static boolean isNotEmptyObj(Object obj)  
    {  
        return !isEmptyObj(obj);  
    }  
	
	/** 首字母转换为大写 **/
	public static String toUpperCaseFirst(String text) {
		return text.substring(0, 1).toUpperCase() + text.substring(1);
	}

	/** 首字母转换为小写 **/
	public static String toLowerCaseFirst(String text) {
		return text.substring(0, 1).toLowerCase() + text.substring(1);
	}
	
	
	/**
	 * 全部大写
	 * 方法名： toUpperCase <BR>
	 * @param str 传入的字符串
	 * @return String <BR>
	 */
	public static String toUpperCase(String str){
		return str.toUpperCase();
	}
	
	/**
	 * 全部小写
	 * 方法名： toLowerCase <BR>
	 * @param str 传入的字符串
	 * @return String <BR>
	 */
	public static String toLowerCase(String str){
		return str.toLowerCase();
	}
	
	/**
	 * @作用:判断字符串是否是数字组成
	 */
	public static boolean isNumeric(String str) {
		Matcher isNum = Pattern.compile("(-|\\+)?[0-9]+(.[0-9]+\\+)?").matcher(
				str);
		return isNum.matches();
	}
	
	/**
	 * 统计单个字符在字符串中出现的次数
	 * @param string  字符串        
	 * @param c  单个字符串    
	 * @return int 返回次数
	 */
	public static int counter(String string, char c) {
		int count = 0;
		for (int i = 0; i < string.length(); i++) {
			if (string.charAt(i) == c) {
				count++;
			}
		}
		return count;
	}
	
	/**
	 * 统计在字符串中  --中文个数，
	 * @param s  字符串    
	 * @return int 返回中文出现的次数
	 * @throws UnsupportedEncodingException
	 */
	public static int getChineseCount(String s)
			throws UnsupportedEncodingException {
		char c;
		int chineseCount = 0;
		if (!"".equals("")) {
			try {
				s = new String(s.getBytes(), "GBK");
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
		}
		for (int i = 0; i < s.length(); i++) {
			c = s.charAt(i);
			if (isChineseChar(c)) {
				chineseCount++;
			}
		}
		return chineseCount;
	}

	
	/**
	 * 统计在字符串中    ---空白数，字母，数字  出现次数
	 * 方法名： StringCountBlankNumCharacter <BR>
	 * @param s
	 * @return String <BR>
	 * @since  1.0.0
	 */
	public static String StringCountBlankNumCharacter(String s) {
		char ch;
		int character = 0, blank = 0, number = 0, other = 0;
		for (int i = 0; i < s.length(); i++) {
			ch = s.charAt(i);
			if (ch >= 'a' && ch <= 'z' || ch >= 'A' && ch <= 'Z') {
				character++;
			} else if (ch == ' ') {
				blank++;
			} else if (ch >= '0' && ch <= '9') {
				number++;
			}
		}
		return "在字符串共有:  【" + character + "】个字母,  【" + blank + "】个空格,  【" + number
				+ "】个数字,  其他字符占有:" + other;
	}
	
	public static boolean isChineseChar(char c)
			throws UnsupportedEncodingException {
		return String.valueOf(c).getBytes("GBK").length > 1;
	}
	
	/**
	 * 截取不要带汉字的字符串 
	 * 方法名： subStringContainChinese <BR>
	 * @param strings  传入的字符串
	 * @param copyNum  截取的长度
	 * @return String <BR>
	 */
	public static String subStringContainChinese(String strings, int copyNum) {
		String[] arr = strings.split("");
		strings = "";// 清空，用于存放已截取的字符串
		int cutNum = 0;
		int cc = 0;
		for (int i = 0; i < arr.length; i++) {
			if (arr[i].getBytes().length == 1) {
				cutNum = cutNum + 1;
				strings = strings + arr[i];
			} else if (arr[i].getBytes().length == 2) {
				cc = cc + 1;
				cutNum = cutNum + 2;
				strings = strings + arr[i];
			}
			if (cutNum >= copyNum) {
				break;
			}
		}
		if (cutNum > copyNum) {
			return strings.substring(0, copyNum - cc);
		} else {
			return strings;
		}
	}
	
	/**
	 * 去掉字符串 左右空格
	 * 方法名： trim <BR>
	 * @param str
	 * @return String <BR>
	 */
	public static String trim(String str) {
		return str == null ? null : str.trim();
	}
	
	
	/**替换字符串
	 * 方法名： replace <BR>
	 * @param line 字符串      
	 * @param oldString 替换字符串中的某字符
	 * @param newString 替换后的新字符    
	 * @return String <BR>
	 */
	public static final String replace(String line, String oldString,
			String newString) {
		if (null == line && "".equals(line)) {
			return null;
		}
		int i = 0;
		if ((i = line.indexOf(oldString, i)) >= 0) {
			char[] line2 = line.toCharArray();
			char[] newString2 = newString.toCharArray();
			int len = oldString.length();
			StringBuffer buffer = new StringBuffer(line2.length);
			buffer.append(line2, 0, i).append(newString2);
			i += len;
			int j = i;
			while (((i = line.indexOf(oldString, i))) > 0) {
				buffer.append(line2, j, i - j).append(newString2);
				i += len;
				j = i;
			}
			buffer.append(line2, j, line2.length - j);
			return buffer.toString();
		}
		return line;
	}
	
	/**
	 * 替换字符串前面重复字符
	 * 
	 * @param string
	 *            startIntercept("12abc123","123") ==> abc123
	 *            startIntercept("21abc323","123") ==> abc123
	 * @param charRemove 要替换的重复字符
	 * @return 如果字符串为空，直接返回
	 */
	public static String startIntercept(String string, String charRemove) {
		int len = string.length();
		if (isEmpty(string)) {
			return string;
		}
		int start = 0;
		if (charRemove == null) {
			while ((start != len)
					&& Character.isWhitespace(string.charAt(start))) {
				start++;
			}
		} else if (charRemove.length() == 0) {
			return string;
		} else {
			while ((start != len)
					&& (charRemove.indexOf(string.charAt(start)) != -1)) {
				start++;
			}
		}
		return string.substring(start);
	}
	
	/**
	 * 替换字符串后缀的重复字符
	 * 方法名： endIntercept <BR>
	 *      endIntercept("12abc11223","123") ==> 12abc
	 *      endIntercept("12abc32323","23") ==> 12abc
	 * @param str  字符串
	 * @param stripChars   要替换的字符串
	 * @return String <BR>
	 */
	public static String endIntercept(String str, String stripChars) {
		int end;
		if (str == null || (end = str.length()) == 0) {
			return str;
		}

		if (stripChars == null) {
			while ((end != 0) && Character.isWhitespace(str.charAt(end - 1))) {
				end--;
			}
		} else if (stripChars.length() == 0) {
			return str;
		} else {
			while ((end != 0)
					&& (stripChars.indexOf(str.charAt(end - 1)) != -1)) {
				end--;
			}
		}
		return str.substring(0, end);
	}
	
	
	/**
	 * 截取前后包含的字符串
	 * @param str  原始字符串
	 * @param stripChars 要去掉的前后字符串。与顺序无关。
	 *             如：intercept("ac123ab","abc") ==>123
	 *            intercept("acb123acb","abc") ==>123
	 *            intercept("ac123abx","abc") ==>123abx
	 * @return String
	 */
	public static String intercept(String str, String stripChars) {
		if (isEmpty(str)) {
			return str;
		}
		str = startIntercept(str, stripChars);
		return endIntercept(str, stripChars);
	}
	
	
	/**
	 * 替换多个字符串
	 * @param strs 替换前的字符串数组      
	 * @param interceptChars  要替换的字符串
	 * @return String[]
	 */
	public static String[] interceptAll(String[] strs, String interceptChars) {
		int strsLen;
		if (strs == null || (strsLen = strs.length) == 0) {
			return strs;
		}
		String[] newArr = new String[strsLen];
		for (int i = 0; i < strsLen; i++) {
			newArr[i] = intercept(strs[i], interceptChars);
		}
		return newArr;
	}
	
	
	/**
	 * @作用:null 或者 'null' 转换成 ""
	 */
	public static final String nullToEmpty(Object str) {
		return (((null == str) || ("null".equals(str))) ? "" : str.toString());
	}
	
	
	/**
	 * 数字 周数转汉字
	 * 方法名： getWeekChinesee <BR>
	 * 创建人： Flycat  <BR>
	 * 时间： 2015年11月6日-下午3:15:47 <BR>
	 * @param week  传入的数字
	 * @return String <BR>
	 * @since  1.0.0
	 */
	public static String getWeekChinesee(int week){
		String cweek = "";
		if(week==1)cweek = "一";
		if(week==2)cweek = "二";
		if(week==3)cweek = "三";
		if(week==4)cweek = "四";
		if(week==5)cweek = "五";
		if(week==6)cweek = "六";
		if(week==7)cweek = "日";
		return cweek;
	}
	
	/**
	 * 数字 转26个英文字母
	 * 方法名： getCharacter <BR>
	 * 创建人： Flycat  <BR>
	 * 时间： 2015年11月6日-下午3:16:42 <BR>
	 * @param num
	 * @return String <BR>
	 * @since  1.0.0
	 */
	public static String getCharacter(int num){
		String cweek = "";
		if(num==1)cweek = "A";
		if(num==2)cweek = "B";
		if(num==3)cweek = "C";
		if(num==4)cweek = "D";
		if(num==5)cweek = "E";
		if(num==6)cweek = "F";
		if(num==7)cweek = "G";
		if(num==8)cweek = "H";
		if(num==9)cweek = "I";
		if(num==10)cweek = "J";
		if(num==11)cweek = "K";
		if(num==12)cweek = "M";
		if(num==13)cweek = "L";
		if(num==14)cweek = "N";
		if(num==15)cweek = "O";
		if(num==16)cweek = "P";
		if(num==17)cweek = "Q";
		if(num==18)cweek = "R";
		if(num==19)cweek = "S";
		if(num==20)cweek = "T";
		if(num==21)cweek = "U";
		if(num==22)cweek = "V";
		if(num==23)cweek = "W";
		if(num==24)cweek = "X";
		if(num==25)cweek = "Y";
		if(num==26)cweek = "Z";
		return cweek;
	}
	
	
	/**
	 * 将数字转换成对应的中文
	 * @param num
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static String intToChnNumConverter(int num){
	    String resultNumber = null;
	    if(num > 10000 || num < 0){
	        return "";
	    }
	    HashMap chnNumbers = new HashMap();
	    chnNumbers.put(0, "零");
	    chnNumbers.put(1, "一");
	    chnNumbers.put(2, "二");
	    chnNumbers.put(3, "三");
	    chnNumbers.put(4, "四");
	    chnNumbers.put(5, "五");
	    chnNumbers.put(6, "六");
	    chnNumbers.put(7, "七");
	    chnNumbers.put(8, "八");
	    chnNumbers.put(9, "九");
	 
	    HashMap unitMap = new HashMap();
	    unitMap.put(1, "");
	    unitMap.put(10, "十");
	    unitMap.put(100, "百");
	    unitMap.put(1000, "千");
	    int[] unitArray = {1000, 100, 10, 1};
	 
	    StringBuilder result = new StringBuilder();
	    int i = 0;
	    while(num > 0){
	        int n1 = num / unitArray[i];
	        if(n1 > 0){
	            result.append(chnNumbers.get(n1)).append(unitMap.get(unitArray[i]));
	        }
	        if(n1 == 0){
	            if(result.lastIndexOf("零") != result.length()-1){
	                result.append("零");
	            }
	        }
	        num = num % unitArray[i++];
	        if(num == 0){
	            break;
	        }
	    }
	    resultNumber = result.toString();
	    if(resultNumber.startsWith("零")){
	        resultNumber = resultNumber.substring(1);
	    }
	    if(resultNumber.startsWith("一十")){
	        resultNumber = resultNumber.substring(1);
	    }
	    return resultNumber;
	}
	
	
	/**
	 * 将小数格式化成字符串，会进行四舍五入  如：3656.4554==》3656.46
	 * 方法名： formatDoubleToString <BR>
	 * 创建人： Flycat  <BR>
	 * 时间： 2015年11月14日-上午9:44:34 <BR>
	 * @param dou
	 * @param format
	 * @return String <BR>
	 * @since  1.0.0
	 */
	public static String formatDoubleToString(double dou,String format){
		if(isEmpty(format))format="#.##";
		DecimalFormat decimalFormat  = new DecimalFormat(format);
		String string = decimalFormat.format(dou); //四舍五入  逢五进一
		return string;
	}

	/**
	 * float 保留三位小数点
	 * @param f
	 * @return
	 */
	public static String formatFloatToStr(float f){
		DecimalFormat decimalFormat=new DecimalFormat("#0.000");//构造方法的字符格式这里如果小数不足2位,会以0补足.
		return decimalFormat.format(f);//format 返回的是字符串
	}
	
	/**
	 * 百分比转换字符串
	 * 方法名： getPercent <BR>
	 * 创建人： Flycat  <BR>
	 * 时间： 2015年11月14日-上午10:03:34 <BR>
	 * @param num
	 * @param totalCount
	 * @param objects
	 * @return String <BR>
	 * @since  1.0.0
	 */
	public static String getPercent(int num,double totalCount,String...objects){
		String format ="#.##";
		if(objects!=null &&objects.length>0){
			format = objects[0];
		}
		return StringUtil.formatDoubleToString((num/totalCount)*100, format)+"%";
	}
	
	
	/**
	 * 冒泡排序
	 * 方法名： sorts <BR>
	 * 创建人： Flycat  <BR>
	 * 时间： 2015年11月14日-下午3:27:01 <BR>
	 * @param datas 传入的int数组
	 * @param flag  true 降序，false 升序 
	 * @return int[] <BR>
	 * @since  1.0.0
	 */
	public static int[] sorts(int[] datas,boolean flag){
		for (int i = 0; i < datas.length; i++) {//轮询次数
			for(int j=0; j < datas.length-1; j++){//交换次数
				if(flag){ 
					if(datas[j] < datas[j+1]){
						int temp = datas[j];
						datas[j] = datas[j+1];
						datas[j+1] = temp;
					}
				}else{
					if(datas[j] > datas[j+1]){
						int temp = datas[j];
						datas[j] = datas[j+1];
						datas[j+1] = temp;
					}
				}
			}
		}
		return datas;
	}
	
	/**
	 * 格式化日期类util 方法名：formatDate
	 * @param date
	 * @param pattern
	 * @return 返回类型：String
	 * @exception
	 * @since 1.0.0
	 */
	public static String formatDate(Date date, String pattern) {
		if (date != null) {
			String dateString = new SimpleDateFormat(pattern).format(date);
			return dateString;
		} else {
			return "";
		}
	}
	
	

	/**
	 * aaaAndccc  ===> A1a3c3d1n1 
	 * 方法名： countString <BR>
	 * 创建人： Flycat  <BR>
	 * 时间： 2015年11月14日-下午4:55:09 <BR>
	 * @param a
	 * @return String <BR>
	 * @since  1.0.0
	 */
	public static String countString(String a){
		 if(a==null)return null;
		 char []c=a.toCharArray();  
	       TreeMap<String,Integer> m=new TreeMap<String, Integer>();
	       StringBuffer buffer = new StringBuffer();
	       for(int i=0;i<c.length;i++){  
	            String cstr=String.valueOf(c[i]);  
	            if(null!=m.get(cstr)){  
	                int count=m.get(cstr);  
	                m.put(cstr, count+1);  
	            }else{  
	                m.put(cstr,1);  
	            }  
	       }
	       for (Map.Entry<String, Integer> entry : m.entrySet()) {
	    	   buffer.append(entry.getKey()+entry.getValue());
	       }
	       return buffer.toString();
	 }
	
	/**
	 * 产生随机数
	 * 方法名： getRandomString <BR>
	 * 创建人： Flycat  <BR>
	 * 时间： 2016年2月9日-下午9:42:38 <BR>
	 * @param length 随机数长度
	 * @return String <BR>
	 * @since  1.0.0
	 */
	private static String getRandomString(int length) {
		StringBuffer bu = new StringBuffer();
		String[] arr = { "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c",
				"d", "e", "f", "g", "h", "i", "j", "k", "m", "n", "p", "q",
				"r", "s", "t", "u", "v", "w", "x", "y", "z", "A", "B", "C",
				"D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "P",
				"Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };
		Random random = new Random();
		while (bu.length() < length) {
			String temp = arr[random.nextInt(57)];
			if (bu.indexOf(temp) == -1) {
				bu.append(temp);
			}
		}
		return bu.toString();
	}
	
	 /** * 产生随机字符串 * */  
	 public static final String randomString(int length) {  
		if (length < 1) {  
			return null;  
		}  
		char[] randBuffer = new char[length];  
		for (int i = 0; i < randBuffer.length; i++) {  
			randBuffer[i] = numbersAndLetters[strGen.nextInt(61)];  
		}  
		return new String(randBuffer);  
	 }
	   
	 /** * 产生随机数值字符串 * */  
	 public static final String randomNumStr(int length) {  
		if (length < 1) {  
			return null;  
		}  
		char[] randBuffer = new char[length];  
		for (int i = 0; i < randBuffer.length; i++) {  
			randBuffer[i] = numbers[numGen.nextInt(9)];  
		}  
		return new String(randBuffer);
	 } 
	
	/**
	 * 得到DEFAULT_LENGTH位的序列号,长度不足10位,前面补0
	 * @param seq 序号
	 * @param DEFAULT_LENGTH 位数
	 * @return
	 */
    public static String getSequence(long seq,int DEFAULT_LENGTH) {
        String str = String.valueOf(seq);
        int len = str.length();
        if (len >= DEFAULT_LENGTH) {// 取决于业务规模,应该不会到达10
            return str;
        }
        int rest = DEFAULT_LENGTH - len;
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < rest; i++) {
            sb.append('0');
        }
        sb.append(str);
        return sb.toString();
    }

    /**
     * JAVA判断字符串数组中是否包含某字符串元素
     * @param substring 某字符串
     * @param source 源字符串数组
     * @return 包含则返回true，否则返回false
     */
	public static boolean isStrArrIn(String substring, String[] source) {
		System.out.println("111:"+substring);
		if (source == null || source.length == 0) {
			return false;
		}
		for (int i = 0; i < source.length; i++) {
			String aSource = source[i];
			if (aSource.equals(substring)) {
				return true;
			}
		}
		return false;
	}
	
	

    
    //java环境  
    public static void getJavaAll(){ 
        Properties props=System.getProperties(); 
        System.out.println("Java的运行环境版本："+props.getProperty("java.version")); 
        System.out.println("Java的运行环境供应商："+props.getProperty("java.vendor")); 
        System.out.println("Java供应商的URL："+props.getProperty("java.vendor.url")); 
        System.out.println("Java的安装路径："+props.getProperty("java.home")); 
        System.out.println("Java的虚拟机规范版本："+props.getProperty("java.vm.specification.version")); 
        System.out.println("Java的虚拟机规范供应商："+props.getProperty("java.vm.specification.vendor")); 
        System.out.println("Java的虚拟机规范名称："+props.getProperty("java.vm.specification.name")); 
        System.out.println("Java的虚拟机实现版本："+props.getProperty("java.vm.version")); 
        System.out.println("Java的虚拟机实现供应商："+props.getProperty("java.vm.vendor")); 
        System.out.println("Java的虚拟机实现名称："+props.getProperty("java.vm.name")); 
        System.out.println("Java运行时环境规范版本："+props.getProperty("java.specification.version")); 
        System.out.println("Java运行时环境规范供应商："+props.getProperty("java.specification.vender")); 
        System.out.println("Java运行时环境规范名称："+props.getProperty("java.specification.name")); 
        System.out.println("Java的类格式版本号："+props.getProperty("java.class.version")); 
        System.out.println("Java的类路径："+props.getProperty("java.class.path")); 
        System.out.println("加载库时搜索的路径列表："+props.getProperty("java.library.path")); 
        System.out.println("默认的临时文件路径："+props.getProperty("java.io.tmpdir")); 
        System.out.println("一个或多个扩展目录的路径："+props.getProperty("java.ext.dirs")); 
        System.out.println("操作系统的名称："+props.getProperty("os.name")); 
        System.out.println("操作系统的构架："+props.getProperty("os.arch")); 
        System.out.println("操作系统的版本："+props.getProperty("os.version")); 
        System.out.println("文件分隔符："+props.getProperty("file.separator"));//在 unix 系统中是＂／＂ System.out.println("路径分隔符："+props.getProperty("path.separator"));//在 unix 系统中是＂:＂ System.out.println("行分隔符："+props.getProperty("line.separator"));//在 unix 系统中是＂/n＂ System.out.println("用户的账户名称："+props.getProperty("user.name"));  
        System.out.println("用户的主目录："+props.getProperty("user.home")); 
        System.out.println("用户的当前工作目录："+props.getProperty("user.dir")); 
    }
	
	/**
	 * 传入年份得到12个月
	 * @param year
	 * @param day
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static List<String> get12MonthList(String year,String day){
		List monthList = new ArrayList();
		for (int i = 1; i <= 12; i++) {
			if(i>=10){
				monthList.add(year+"-"+i+"-"+day);
			}else{
				monthList.add(year+"-0"+i+"-"+day);
			}
		}
		System.out.println("monthList:"+monthList);
		return monthList;
	}
	
	/**
	 * 得到指定日期的所有天
	 * 例：2018-02
	 * @param monthDay
	 * @param day
	 * @return
	 * @throws Exception
	 */
	public static List<String> getAllDaysMonthByDate(String monthDay,String day) throws Exception{
		List<String> monthList = new ArrayList();
		monthDay = monthDay+"-01";
		Date d = sdf.parse(monthDay);
		monthList = DateUtil.getAllDaysMonthByDate(d);
		return monthList;
	}
	
	/**
	 * 得到指定日期的所有小时
	 * @param dateDay
	 * @param day
	 * @return
	 * @throws Exception
	 */
	public static List<String> getAllHours(String dateDay,String day) throws Exception{
		List<String> dayList = new ArrayList();
		for (int i = 0; i < 24; i++) {
			if(i>=10){
				dayList.add(dateDay+" "+i+":"+day);
			}else{
				dayList.add(dateDay+" 0"+i+":"+day);
			}
		}
		return dayList;
	}
	
	/**
	 * String returnString=changeInputStream(httpCon.getInputStream(), "UTF-8");
       logger.info("returnString:"+returnString);
       JSONObject reObject=JSONObject.fromObject(returnString);
	 * @param inputStream
	 * @param encode
	 * @return
	 */
   	public static String changeInputStream(InputStream inputStream,String encode) {  
        // 内存流  
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();  
        byte[] data = new byte[1024];  
        int len = 0;  
        String result = null;  
        if (inputStream != null) {  
            try {  
                while ((len = inputStream.read(data)) != -1) {  
                    byteArrayOutputStream.write(data, 0, len);  
                }  
                result = new String(byteArrayOutputStream.toByteArray(), encode);  
            } catch (IOException e) {  
                e.printStackTrace();  
            }  
        }  
        return result;  
    }
	
	/**
	 * 将base64二进制流转为pdf
	 * @param base64Content	二进制流
	 * @param filePath	文件路径
	 * @param fileName	文件名
	 */
	 public static void base64StringToPdf(String base64Content,String filePath, String fileName){
        BASE64Decoder decoder = new BASE64Decoder();
        BufferedInputStream bis = null;
        FileOutputStream fos = null;
        BufferedOutputStream bos = null;
        try {
            byte[] bytes = decoder.decodeBuffer(base64Content);//base64编码内容转换为字节数组
            ByteArrayInputStream byteInputStream = new ByteArrayInputStream(bytes);
            bis = new BufferedInputStream(byteInputStream);
            String realPath = filePath+"\\"+fileName+".pdf";
            System.out.println("realPath:"+realPath);
            //文件名
            File file = new File(realPath);
            fos = new FileOutputStream(file);
            bos = new BufferedOutputStream(fos);
            byte[] buffer = new byte[1024];
            int length = bis.read(buffer);
            while(length != -1){
                bos.write(buffer, 0, length);
                length = bis.read(buffer);
            }
            bos.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }finally{
        	try {
				bis.close();
				fos.close();
				bos.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
        }
    }
	
	/**
	 * 打开EXE执行文件
	 * @param exeFile
	 */
	public static void openExePrint(String exeFile,String pdfPath) {  
	    final Runtime runtime = Runtime.getRuntime();  
	    Process process = null;  
	    try {  
	    	if(StringUtil.isNotEmpty(pdfPath)){
	    		process = runtime.exec(exeFile+" "+pdfPath);  
	    	}else{
	    		process = runtime.exec(exeFile);
	    	}
	        process = runtime.exec(exeFile+" "+pdfPath);  
	    } catch (final Exception e) {  
	        System.out.println("Error exec!");  
	    }  
	}
	
	public static void printList(List<String> list) {
		for (String string : list) {
			System.out.println(string);
		}
	}

	//测试类
	public static void main(String[] args) throws Exception {

		//1.非空判断
		/*String str ="分公司打工";
		boolean flag =isNotEmpty(str);
		if(flag){
			System.out.print(flag+"  success...");
		}else{
			System.out.print(flag+"  error...");
		}*/
		
		//2.判断字符串是否为空
		/*String str =" ";
		boolean flag =isEmpty(str);
		if(flag){
			System.out.print(flag+"  success...");
		}else{
			System.out.print(flag+"  error...");
		}*/
		
		//3.首字母转换为大写
		/*String str ="asaseF2343";
		String text =toUpperCaseFirst(str);
		System.out.println(text);*/
		
		//4.首字母转换为小写
		/*String str ="FasAD发生的2343";
		String text =toLowerCaseFirst(str);
		System.out.println(text);*/
		
		//5.全部大写
		/*String str ="FasAasdDsdda";
		String text =toUpperCase(str);
		System.out.println(text);*/
		
		//6.全部小写
		/*String str ="FasAasdDsdda";
		String text =toLowerCase(str);
		System.out.println(text);*/
		
		//7.判断字符串 是否为数字
		/*String str ="412342";
		boolean flag =isNumeric(str);
		if(flag){
			System.out.print(flag+"  success...");
		}else{
			System.out.print(flag+"  error...");
		}*/
		
		//8.统单个字符在字符串中出现的次数
		/*String string="21342wfeesfdfggdd517`2123";
		char c='2';
		int  count =counter(string,c);
		System.out.print(count+" 次");*/
		
		//9.统计在字符串中--汉字出现的次数
		/*String s="da萨蒂sdaa阿什顿daa11";
		try {
			int ChineseCount =getChineseCount(s);
			System.out.print(ChineseCount+" 次");
		} catch (UnsupportedEncodingException e) {
			System.out.print("error...");
			e.printStackTrace();
		}*/
		
		//10.统计在字符串中    ---空白数，字母，数字  出现次数
		/*String s ="adaqeeq 23sad 21 difhas司法会计dfd";
		String count=StringCountBlankNumCharacter(s);
		System.out.println(count);*/
		
		//11.截取不带汉字的字符串
		/*String strings="adf阿什顿番企鹅QWE阿三sfg523sdf";
		int copyNum=6;
		String ChineseStr =subStringContainChinese(strings,copyNum);
		System.out.println(ChineseStr);*/
		
		//12.去掉字符串左右空格
		/*String str ="  sdadsafiy啊说法as  ";
		String trim =trim(str);
		System.out.println(trim);*/
		
		//13.替换字符串
		/*String line="3asdaafsdaf21aadfj安排阿萨斯的fdss"; 
		String oldString="a"; //原替换字符串中的某个字符
		String newString="1"; //替换后的新字符
		String replace =replace(line,oldString,newString);
		System.out.println(replace);*/
		
		//14. 替换字符串前面重复字符 
		/*String string="sdsdfffDwwwHESasdASD";
		String charRemove="sdf";
		String startIntercept = startIntercept(string,charRemove);
		System.out.print(startIntercept);*/
		
		//15. 替换字符串后面的重复字符
		/*String str ="as1aU3IAHf3323233"; 
		String stripChars ="23"; //退换字符串后重复字符
		String endIntercept = endIntercept(str,stripChars);
		System.out.print(endIntercept);*/
		
		//16.截取前后包含的字符串
		/*String str ="asfasfDeefgMdfDaasaf";
		String stripChars="saf";
		String intercept =intercept(str,stripChars);
		System.out.println(intercept);*/
		
		//17.替换多个字符
		/*String[] strs={"qw","sd","sd","qw","sd","23","423","fg","sd","sdf","df"};
		String interceptChars="sd";
		String[] interceptAll =interceptAll(strs,interceptChars);
		for(int i =0;i<interceptAll.length;i++){
			System.out.print(interceptAll[i]+" ");
		}*/
		
		//18. 数字周数 转 汉字
		/*System.out.println("请输入 1-7 周数  的数字 --转汉字 ：");
		int week= input.nextInt();
		String getWeekChinesee =getWeekChinesee(week);
		System.out.println(getWeekChinesee);*/
		
		//19.数字 转26 个英文字母
		/*System.out.println("请输入1-26  的数字 --转英文字母：");
		int num=input.nextInt();
		String getCharacter =getCharacter(num);
		System.out.println(getCharacter);*/
		
		//20. 数字转换成对应的中文
		/*System.out.println("请输入数字 --转对应的汉字：");
		int num=input.nextInt();
		String intToChnNumConverter =intToChnNumConverter(num);
		System.out.println(intToChnNumConverter);*/
		
		//21.将小数格式化成字符串
		/*double dou = 12.573;
		String format="";
		String formatDoubleToString =formatDoubleToString(dou,format);
		System.out.println(formatDoubleToString);*/
		
		//22.百分比转换字符串
		/*String getPercent =getPercent(2,2,"");
		System.out.println(getPercent);*/
		
		//23.冒泡排序  true 降序   false 升序
		/*int[] datas={2,3,6,13,22,11,31,1,7};
		int[] sorts =sorts(datas,false);
		
		//int 数组转==》  String 数组 ==》String
		String[] sstr = new String[sorts.length];
		for(int i=0; i<sorts.length; i++){
		   sstr[i] = String.valueOf(sorts[i]);
		   System.out.print(sstr[i]+" ");
		}*/
		
		


		
		//BASE64Encoder 加密
		/*byte[] b={41,2,62,23};
		String base64Encode =base64Encode(b);
		System.out.println("base64Encode :"+base64Encode);*/
		
		//27. aaaAndccc  ===> A1a3c3d1n1 
		/*String a="aaaAndccc";
		String countString =countString(a);
		System.out.println(countString+" ");*/
		
		/*long start = System.currentTimeMillis();
		for (int i = 1; i <= 1000; i++) {
			System.out.println("====="+i);
		}
		long end = System.currentTimeMillis();
		System.out.println("一共耗费:"+(end-start)+"毫秒");*/
		
		//输出 26个 大小写英文 字母   例如 ：A a B b C c....
		/*for(int i=0;i<26;i++){
			System.out.print((char)('A'+i)+" ");
			System.out.print((char)('a'+i)+" ");
		}*/
		
		String str = "房估字(2014)第+YPQD0006号";
    	String jieguo = str.substring(str.indexOf("+")+1,str.indexOf("号"));  	
    	System.out.println("截取【+】后 --【号】前  ==>"+jieguo);
    	
    	String num1=str.substring(0,str.indexOf("+"));
    	System.out.println("截取【+】号前面  ==》num1  ==>"+num1);
    	
    	String num2=str.substring(str.indexOf("+")+1);
    	System.out.println("截取【+】号后面  ==》num2 ==>"+num2);
    	
		String weight ="1.5765";
		System.out.println("weight"+StringUtil.formatDoubleToString(Double.parseDouble(weight), "#.###"));
		System.out.println("weight"+StringUtil.formatFloatToStr(Float.parseFloat(weight)));
		
		getJavaAll();
		//当前系统名称
	    String OS_NAME = System.getProperty("os.name").toLowerCase(Locale.US);
	    System.out.println(OS_NAME);
	    //当前系统的位数X86?X64
	    String OS_ARCH = System.getProperty("os.arch").toLowerCase(Locale.US);
	    System.out.println(OS_ARCH);
	    //获取当前系统的版本信息
	    String OS_VERSION = System.getProperty("os.version").toLowerCase(Locale.US);
	    System.out.println(OS_VERSION);
	    
	    //获取Tomcat 服务器地址
	    String TomcatPath =System.getProperty("user.dir");
	    //获取所有的类路径 包括jar包的路径
	    String allClassPath = System.getProperty("java.class.path");
	    System.out.println("22 "+TomcatPath);
	    System.out.println(allClassPath);
	   
	    
	    //项目服务器脚本文件路径
	    File directory = new File("");//设定为当前文件夹 
	    System.out.println(directory.getCanonicalPath());//获取标准的路径 
	    System.out.println(directory.getAbsolutePath());//获取绝对路径 
		
	    System.out.println(get12MonthList("2017","02"));
	    System.out.println(getAllDaysMonthByDate("2017-02","01"));
	    System.out.println(getAllHours("2017-02-28", "00:00"));
	    	
	    String sss ="2017-02-28 01:00:00";
    	String ss1=sss.substring(0,sss.indexOf(" "));
    	ss1 = ss1.replace("-", "");
    	String ss2=sss.substring(sss.indexOf(":")+1);
    	System.out.println(ss1+"======"+ss2);
    	
//    	
//    	String password ="";
//    	String passwordmd5 ="";
//    	for (int i = 1; i <= 20; i++) {
//    		password ="wisesh0";
//			if(i>=10){
//				password =password+i;
//			}else{
//				password =password+"0"+i;
//			}
//			passwordmd5 =StringUtil.getMD5LowerCase(StringUtil.toLowerCase(password));
//			System.out.println(password+"<=====>"+passwordmd5);
//		}
    	
	    
	    //项目部署的路径
	    //String path = request.getSession().getServletContext().getRealPath("/"); 
	}
}
