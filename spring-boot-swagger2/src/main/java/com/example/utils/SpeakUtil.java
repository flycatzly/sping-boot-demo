package com.example.utils;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLConnection;

//import net.sf.json.JSONObject;

//import org.apache.log4j.Logger;
public class SpeakUtil {
	//public static Logger logger = Logger.getLogger(SpeakUtil.class);
	
	// 设置APPID/AK/SK
	public static final String APP_ID = "11021939";
	public static final String API_KEY = "f8CqdzIGSal9c6SNi0jh7PD6";
	public static final String SECRET_KEY = "1KwTpuBKn7gf58NmuPTGaPt21QGhgZ0I";
	
	
	/**
	 * 语音播报
	 * 获取 access_token
	 * @return
	 * @throws IOException
	 */
	public static String getAccessToken(){
		String getTokenAPI = "https://openapi.baidu.com/oauth/2.0/token?grant_type=client_credentials&client_id="
				+ API_KEY + "&client_secret=" + SECRET_KEY;

		String returnString =null;
		String accessToken =null;
		returnString =sendPost(getTokenAPI, "");
		System.out.println(returnString);
//		JsonUtils reObject = JSONObject.fromObject(returnString);
//		if(StringUtil.isNotEmptyObj(reObject)){
//			//logger.info("获取语音播报\n"+reObject);
//			accessToken =reObject.get("access_token")+"";
//		}else{
//			accessToken ="";
//		}
		return accessToken;
	}
	
 
	/**  
     * 向指定 URL 发送POST方法的请求  
     * @url    发送请求的 URL  
     * @param  请求参数，请求参数应该是 name1=value1&name2=value2 的形式。  
     * @return 所代表远程资源的响应结果  
     */ 
    public static String sendPost(String url, String param) {  
  
        PrintWriter out = null;  
        BufferedReader in = null;  
        StringBuilder result = new StringBuilder();  
        try {  
            URL realUrl = new URL(url);  
            // 打开和URL之间的连接  
            URLConnection conn = realUrl.openConnection();  
            //设置连接超时
            conn.setConnectTimeout(20000);
			conn.setReadTimeout(20000);
            // 设置通用的请求属性  
            conn.setRequestProperty("accept", "*/*");  
            conn.setRequestProperty("connection","Keep-Alive");  
            conn.setRequestProperty("user-agent","Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");  
            conn.setRequestProperty("Accept-Charset", "UTF-8");  
            conn.setRequestProperty("contentType", "UTF-8");  
            // 发送POST请求必须设置如下两行  
            conn.setDoOutput(true);  
            conn.setDoInput(true);  
            // 获取URLConnection对象对应的输出流  
            out = new PrintWriter(conn.getOutputStream());  
            // 发送请求参数  
            out.print(param);  
            // flush输出流的缓冲  
            out.flush();  
            // 定义BufferedReader输入流来读取URL的响应  
            in = new BufferedReader(new InputStreamReader(conn.getInputStream()));  
            String line;  
            while ((line = in.readLine()) != null) {  
                result.append(line);  
            }  
        }catch (Exception e) {  
            result = new StringBuilder("");  
            e.printStackTrace();  
            //logger.error("语音播报获取key异常！"+e);
        }finally {  
        	// 使用finally块来关闭输出流、输入流 
            try {  
                if (out != null) {  
                    out.close();  
                }  
                if (in != null) {  
                    in.close();  
                }  
            } catch (IOException ex) {  
                ex.printStackTrace();  
            }  
        }  
        return result.toString();  
    } 
	
	public static void main(String[] args){
		System.out.println(getAccessToken());
	}
	
}
