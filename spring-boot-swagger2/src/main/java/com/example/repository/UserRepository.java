package com.example.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.example.entity.User;

//public interface UserRepository extends JpaRepository<User, Long> {
//自定义sql
public interface UserRepository extends JpaRepository<User, Long>,JpaSpecificationExecutor<User>,Serializable{

    User findById(long id);

    User findByUserName(String userName);
    
    User findByUserNameAndPassword(String userName, String passWord);
    
//    Page<User> findALL(Pageable pageable);
    
	List<User> findByUserNameIn(List<String> UserNameList);
	
	@Transactional
	@Modifying
	@Query("delete from User where userName = ?1")
	void deleteByUserName(String userName);
	
	@Transactional
	@Modifying
	@Query("delete from User where id = ?1")
	void deleteById(long id);

}
