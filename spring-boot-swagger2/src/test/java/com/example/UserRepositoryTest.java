package com.example;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.entity.User;
import com.example.repository.UserRepository;
import com.example.utils.DateUtil;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserRepositoryTest {
	
	@Autowired
	private UserRepository userRepository;
	
	@Test
	public void testBaseQuery() throws Exception {
		System.out.println(userRepository.count());
		System.out.println(userRepository.findAll());
	}
	
	@Test
	public void findByUserName() throws Exception {
		User user =userRepository.findByUserName("zly");
		System.out.println(user);
	}
	
	@Test
	public void findByUserNameAndPassword() throws Exception {
		User user =userRepository.findByUserNameAndPassword("999","999");
		System.out.println(user);
	}
	
	/**
	 * 测试In查询
	 */
	@Test
	public void findByUserNameInTest(){
		List<String> list = new ArrayList<String>();
		list.add("99");
		list.add("88");
		list.add("77");
		List<User> result =userRepository.findByUserNameIn(list);
		System.out.println(result.size()+"\t"+result);
		Assert.assertNotEquals(0, result.size());
		
	}
	
	@Test
	public void insert() throws Exception {
		User user=new User();
		user.setId(11);
		user.setUserName(DateUtil.getNowTimeStamp()+"_Name");
		user.setPassword(DateUtil.getNowTimeStamp()+"_password");
		user.setAge(99);
		userRepository.save(user);
		System.out.println(userRepository.count());
	}
	
	@Test
	public void update() throws Exception {
		User user=new User();
		user.setId(2L);
		user.setUserName("zly1");
		user.setPassword("1111");
		user.setAge(111);
		userRepository.save(user);
		System.out.println(userRepository.count());
	}
	
	@Test
	public void delete() throws Exception {
		userRepository.deleteById(2L);
		System.out.println(userRepository.count());
	}
	
	/**
	 * 自定义sql byID 删除
	 * @throws Exception
	 */
	@Test
	public void deleteById() throws Exception {
		userRepository.deleteById(7);
		userRepository.deleteById(8);
		userRepository.deleteById(9);
		System.out.println(userRepository.count());
	}
	
	/**
	 * 自定义sql by name 删除
	 * @throws Exception
	 */
	@Test
	public void deleteByName() throws Exception {
		userRepository.deleteByUserName("4444");
		System.out.println(userRepository.count());
	}
	
}
