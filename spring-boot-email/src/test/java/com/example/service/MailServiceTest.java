package com.example.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.bind.annotation.RequestMapping;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import com.example.pojo.common.IZlyJSONResult;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MailServiceTest {

    @Autowired
    private IMailService MailService;
    @Autowired
    private TemplateEngine templateEngine;
    
    @Test
    public void home() throws Exception {
    	
    	String emailName="469219622@qq.com";
    	for (int i = 0; i < 100; i++) {
//    		Thread.sleep(2000);
        	index();
        	htmlEmail();
        	attachmentsMail();
        	resourceMail();
//        	templateMail();
//        	Thread.sleep(1000);
        	System.out.println("发送邮件给 "+emailName+" 成功！"+i);
		}
    }

    
    @Test
    public void index(){
    	String emailName="2286704940@qq.com";
    	String mailTitle ="渣渣聘1，这是一封普通的邮件";
		String mailContent="渣渣聘1，这是一封普通的SpringBoot测试邮件!";
		MailService.sendSimpleMail(emailName,mailTitle,mailContent);
//		System.out.println("发送邮件给 "+emailName+" 成功！");
    }
    
    @Test
    public void htmlEmail(){
    	String emailName="2286704940@qq.com";
    	String mailTitle ="渣渣聘，这是一HTML的邮件";
    	String mailContent="<body style=\"text-align: center;margin-left: auto;margin-right: auto;\">\n"
                + "	<div id=\"welcome\" style=\"text-align: center;position: absolute;\" >\n"
                +"		<h3>欢迎使用IJPay -By Javen</h3>\n"
                +"		<span>https://github.com/</span>"
                + "		<div\n"
                + "			style=\"text-align: center; padding: 10px\"><a style=\"text-decoration: none;\" href=\"D:\\1\\app.jpg\" target=\"_bank\" ><strong>塘桥夜话 :)</strong></a></div>\n"
                + "		<div\n" + "			style=\"text-align: center; padding: 4px\">渣渣聘，,请任意打赏</div>\n"
                + "		<img width=\"180px\" height=\"180px\"\n"
                + "			src=\"D:\\1\\app.jpg\">\n"
                + "	</div>\n" + "</body>";
        try {
        	MailService.sendHtmlMail(emailName,mailTitle,mailContent);
        }catch (Exception ex){
            ex.printStackTrace();
//            System.out.println(IZlyJSONResult.build(-1, "邮件发送失败!!", ex));
        }
//        System.out.println(IZlyJSONResult.ok("发送邮件给 "+emailName+" 成功！"));
    }
    
    @Test
    public void attachmentsMail(){
        String filePath = "D:\\1\\app.jpg";
        String emailName="2286704940@qq.com";
        String mailTitle ="这是一封带附件的邮件";
        String mailContent="邮件中有附件，请注意查收！";
    	try {
    		MailService.sendAttachmentsMail(emailName, mailTitle, mailContent, filePath);
        }catch (Exception ex){
            ex.printStackTrace();
//            System.out.println(IZlyJSONResult.build(-1, "邮件发送失败!!", ex));
        }
//    	System.out.println(IZlyJSONResult.ok("发送邮件给 "+emailName+" 成功！"));
    }
    
    @Test
    public void resourceMail(){
        String rscId = "FlycatZly";
        String imgPath = "D:/1/app.jpg";
        String emailName="2286704940@qq.com";
        String mailTitle ="这邮件中含有图片";
        String mailContent="<html><body>这是有图片的邮件<br/><img src=\'cid:" + rscId + "\' ></body></html>";
    	try {
    		MailService.sendResourceMail(emailName, mailTitle, mailContent, imgPath, rscId);
        }catch (Exception ex){
            ex.printStackTrace();
//            System.out.println(IZlyJSONResult.build(-1, "邮件发送失败!!", ex));
        }
//    	System.out.println(IZlyJSONResult.ok("发送邮件给 "+emailName+" 成功！"));
    }
    
    @Test
    public void templateMail(){
        Context context = new Context();
        context.setVariable("project", "IJPay");
        context.setVariable("author", "Javen");
        context.setVariable("url", "https://github.com/");
        
        String emailName="2286704940@qq.com";
        String mailTitle ="这是模板邮件";
        String mailContent = templateEngine.process("emailTemp", context);
        try {
        	MailService.sendHtmlMail(emailName, mailTitle, mailContent);
        }catch (Exception ex){
            ex.printStackTrace();
//            System.out.println(IZlyJSONResult.build(-1, "邮件发送失败!!", ex));
        }
//        System.out.println(IZlyJSONResult.ok("发送邮件给 "+emailName+" 成功！"));
    }
    
    
}
