package com.example.contoller.test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.pojo.Resource;
import com.example.pojo.User;

@Controller
@RequestMapping("thy")
public class ThymeleafContoller {
	
	@Autowired
	private Resource resource;
	
	@RequestMapping("/index")
    public String index(ModelMap map) {
        map.addAttribute("name", "Hello Thymeleaf !!!");
        return "thymeleaf/index";
    }
	
	@RequestMapping("center")
    public String center() {
        return "thymeleaf/center/center";
    }
	
	@RequestMapping("/test")
    public String test(ModelMap map) {
    	User user = new User();
    	user.setName("FlycatZly");
    	user.setPassword("621014");
    	user.setAge(18);
    	user.setBirthday(new Date());
    	user.setDesc("<font color='green'><b>Hello FlycatZly !!! </b></font>");
    	map.addAttribute("user", user);
    	
    	User user1 = new User();
    	user1.setName("Fly");
    	user1.setPassword("621012");
    	user1.setAge(20);
    	user1.setBirthday(new Date());
    	user1.setDesc("Hello Fly ~");
    	
    	User user2 = new User();
    	user2.setName("Zly");
    	user2.setPassword("123456");
    	user2.setAge(16);
    	user2.setBirthday(new Date());
    	user1.setDesc("Hello Zly ~");
    	
		List<User> userList = new ArrayList<>();
		userList.add(user1);
		userList.add(user2);
		userList.add(user);
		map.addAttribute("userList", userList);
    	
		return "thymeleaf/test";
    }
	
	@RequestMapping("/postForm")
	public String postForm(User user){
    	System.out.println(user.getName()+"\t"+user.getAge());
		return "redirect:/thy/test";
	}
	
}
