package com.example.contoller.test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import com.example.pojo.common.IZlyJSONResult;
import com.example.service.impl.IMailServiceImpl;

@RestController
@RequestMapping("/email")
public class EmailController {

    @Autowired
    private IMailServiceImpl mailService;
    @Autowired
    private TemplateEngine templateEngine;

    /**
     * 1.发送普通邮件
     * @param emailName
     * @return
     */
	@RequestMapping("/")
    public IZlyJSONResult one(){
    	String emailName="2286704940@qq.com";
        return index(emailName, "", "");
//        return htmlEmail(emailName, "", "");
//        return attachmentsMail(emailName, "", "");
//        return resourceMail(emailName, "", "");
//        return templateMail(emailName, "", "");
    }
	
	@RequestMapping("/index")
    public IZlyJSONResult index(String emailName,String mailTitle,String mailContent){
		mailTitle ="这是一封普通的邮件";
		mailContent="这是一封普通的SpringBoot测试邮件!";
		try {
            mailService.sendSimpleMail(emailName,mailTitle,mailContent);
        }catch (Exception ex){
            ex.printStackTrace();
            return IZlyJSONResult.build(-1, "邮件发送失败!!", ex);
        }
        return IZlyJSONResult.ok("发送邮件给 "+emailName+" 成功！");
    }

	/**
	 * 2.发送HTML邮件
	 * @param emailName
	 * @return
	 */
    @RequestMapping("/htmlEmail")
    public IZlyJSONResult htmlEmail(String emailName,String mailTitle,String mailContent){
    	
		mailTitle ="这是一HTML的邮件";
		mailContent="<body style=\"text-align: center;margin-left: auto;margin-right: auto;\">\n"
                + "	<div id=\"welcome\" style=\"text-align: center;position: absolute;\" >\n"
                +"		<h3>欢迎使用IJPay -By Javen</h3>\n"
                +"		<span>https://github.com/Javen205/IJPay</span>"
                + "		<div\n"
                + "			style=\"text-align: center; padding: 10px\"><a style=\"text-decoration: none;\" href=\"https://github.com/Javen205/IJPay\" target=\"_bank\" ><strong>IJPay 让支付触手可及,欢迎Start支持项目发展:)</strong></a></div>\n"
                + "		<div\n" + "			style=\"text-align: center; padding: 4px\">如果对你有帮助,请任意打赏</div>\n"
                + "		<img width=\"180px\" height=\"180px\"\n"
                + "			src=\"https://oscimg.oschina.net/oscnet/8e86fed2ee9571eb133096d5dc1b3cb2fc1.jpg\">\n"
                + "	</div>\n" + "</body>";
        try {
            mailService.sendHtmlMail(emailName,mailTitle,mailContent);
        }catch (Exception ex){
            ex.printStackTrace();
            return IZlyJSONResult.build(-1, "邮件发送失败!!", ex);
        }
        return IZlyJSONResult.ok("发送邮件给 "+emailName+" 成功！");
    }

    /**
     * 3.发送附件邮箱
     * @param emailName
     * @return
     */
    @RequestMapping("/attachmentsMail")
    public IZlyJSONResult attachmentsMail(String emailName,String mailTitle,String mailContent){
        String filePath = "C:\\image\\10214103.jpg";
		mailTitle ="这是一封带附件的邮件";
		mailContent="邮件中有附件，请注意查收！";
    	try {
            mailService.sendAttachmentsMail(emailName, mailTitle, mailContent, filePath);
        }catch (Exception ex){
            ex.printStackTrace();
            return IZlyJSONResult.build(-1, "邮件发送失败!!", ex);
        }
        return IZlyJSONResult.ok("发送邮件给 "+emailName+" 成功！");
    }

    /**
     * 4.有图片的邮件
     * @param emailName
     * @return
     */
    @RequestMapping("/resourceMail")
    public IZlyJSONResult resourceMail(String emailName,String mailTitle,String mailContent){
        String rscId = "FlycatZly";
        String imgPath = "C:\\image\\10214103.jpg";
		mailTitle ="这邮件中含有图片";
		mailContent="<html><body>这是有图片的邮件<br/><img src=\'cid:" + rscId + "\' ></body></html>";
    	try {
            mailService.sendResourceMail(emailName, mailTitle, mailContent, imgPath, rscId);
        }catch (Exception ex){
            ex.printStackTrace();
            return IZlyJSONResult.build(-1, "邮件发送失败!!", ex);
        }
        return IZlyJSONResult.ok("发送邮件给 "+emailName+" 成功！");
    }

    /**
     * 5.模板邮件
     * @param emailName
     * @return
     */
    @RequestMapping("/templateMail")
    public IZlyJSONResult templateMail(String emailName,String mailTitle,String mailContent){
        Context context = new Context();
        context.setVariable("project", "IJPay");
        context.setVariable("author", "Javen");
        context.setVariable("url", "https://github.com/Javen205/IJPay");
        mailTitle ="这是模板邮件";
        mailContent = templateEngine.process("emailTemp", context);
        try {
            mailService.sendHtmlMail(emailName, mailTitle, mailContent);
        }catch (Exception ex){
            ex.printStackTrace();
            return IZlyJSONResult.build(-1, "邮件发送失败!!", ex);
        }
        return IZlyJSONResult.ok("发送邮件给 "+emailName+" 成功！");
    }
}
