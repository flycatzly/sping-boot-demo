package com.example.contoller.test;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.pojo.common.IZlyJSONResult;

@Controller
@RequestMapping("/err")
public class ErrorController {
	
    @RequestMapping("/error")
    public String error(){
    	int a =1/0;
    	return "thymeleaf/error";
    }
    
    @RequestMapping("/ajaxError")
    public String ajaxError(){
    	int a =1/0;
    	return "thymeleaf/ajaxError";
    }

    @RequestMapping("/getAjaxError")
    public IZlyJSONResult getAjaxError(){
    	int a =1/0;
    	return IZlyJSONResult.ok();
    }

}
