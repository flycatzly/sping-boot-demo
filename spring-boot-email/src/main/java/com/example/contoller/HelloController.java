package com.example.contoller;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.pojo.Resource;
import com.example.pojo.common.IZlyJSONResult;

@RestController
public class HelloController {
	
	@Autowired
	private Resource resource;
	
    @RequestMapping("/hello")
    public Object hello() {
		return "hello SpringBoot~!";
    }
    
    @RequestMapping("/getHello")
    public String hello(Model model, @RequestParam(value="name", required=false, defaultValue="World") String name) {
        model.addAttribute("name", name);
        return "hello "+name;
    }
    
    @RequestMapping("/getResource")
    public IZlyJSONResult getResource(){
    	Resource bean =new Resource();
    	BeanUtils.copyProperties(resource, bean);
		return IZlyJSONResult.ok(bean);
    }
    
}
