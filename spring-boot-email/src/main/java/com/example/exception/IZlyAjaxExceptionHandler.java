package com.example.exception;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.ModelAndView;

import com.example.pojo.common.IZlyJSONResult;

//@RestControllerAdvice
public class IZlyAjaxExceptionHandler {
	
//	@ExceptionHandler(value=Exception.class)
	public IZlyJSONResult defaultErrorHandler(HttpServletRequest request,HttpServletResponse response,Exception e) throws Exception{
		
		e.printStackTrace();
		return IZlyJSONResult.errorException(e.getMessage());
	}
}
