package com.example.task;

import java.util.concurrent.Future;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("tasks")
public class DoTask {
	
	@Autowired
	private TestAsync testAsync;
	
	@RequestMapping("test")
	public String test() throws Exception{
		long start = System.currentTimeMillis();
		
		Future<Boolean> a1 = testAsync.doTask();
		Future<Boolean> a2 = testAsync.doTask2();
		Future<Boolean> a3 = testAsync.doTask3();
		Future<Boolean> a4 = testAsync.doTask4();
		Future<Boolean> a5 = testAsync.doTask5();
		
		while (!a1.isDone() || !a2.isDone() || !a3.isDone() || !a4.isDone() || !a5.isDone()) {
			if(a1.isDone() &&a2.isDone() && a3.isDone()&& a4.isDone()&& a5.isDone()) {
				break;
			}
		}
		long end =System.currentTimeMillis();
		String msg ="任务全部完成，总耗时："+(end-start)+"毫秒！"; 
		System.out.println(msg);
		return msg;
	}

}
