package com.example.task;

import java.util.concurrent.Future;

import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Component;

/**
 * 异步执行 使用的场景 test
 * 1.发送短信
 * 2.发送邮件
 * 3.App消息推送
 * 4.节省运维凌晨发布任务时间提供效率
 * @author Administrator
 */
@Component
public class TestAsync {
	
	/**
	 * @Async	去掉这个注解会同步执行
	 * @return
	 * @throws Exception
	 */
	@Async
	public Future<Boolean> doTask() throws Exception{
		long start = System.currentTimeMillis();
		Thread.sleep(800);
		long end =System.currentTimeMillis();
		System.out.println("doTask1 正在执行... \t任务耗时："+(end - start)+"毫秒");
		return new AsyncResult<>(true);
	}
	
	@Async
	public Future<Boolean> doTask2() throws Exception{
		long start = System.currentTimeMillis();
		Thread.sleep(600);
		long end =System.currentTimeMillis();
		System.out.println("doTask2 正在执行... \t任务耗时："+(end - start)+"毫秒");
		return new AsyncResult<>(true);
	}
	
	@Async
	public Future<Boolean> doTask3() throws Exception{
		long start = System.currentTimeMillis();
		Thread.sleep(1000);
		long end =System.currentTimeMillis();
		System.out.println("doTask3 正在执行... \t任务耗时："+(end - start)+"毫秒");
		return new AsyncResult<>(true);
	}
	
	@Async
	public Future<Boolean> doTask4() throws Exception{
		long start = System.currentTimeMillis();
		Thread.sleep(1400);
		long end =System.currentTimeMillis();
		System.out.println("doTask4 正在执行... \t任务耗时："+(end - start)+"毫秒");
		return new AsyncResult<>(true);
	}
	
	@Async
	public Future<Boolean> doTask5() throws Exception{
		long start = System.currentTimeMillis();
		Thread.sleep(200);
		long end =System.currentTimeMillis();
		System.out.println("doTask5 正在执行... \t任务耗时："+(end - start)+"毫秒");
		return new AsyncResult<>(true);
	}

}
