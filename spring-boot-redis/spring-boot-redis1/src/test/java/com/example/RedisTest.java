package com.example;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.pojo.SysUser;
import com.example.utils.DateUtil;
import com.example.utils.JsonUtils;
import com.example.utils.RedisUtils;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RedisTest {
	
	@Autowired
	private RedisUtils redis;
	
	@Autowired
    private StringRedisTemplate strRedis;
	
    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    @Test
    @Ignore
    public void saveTest() throws Exception {
    	redis.set("aaa", "111");
    	strRedis.opsForValue().set("bbb", "222");
        redisTemplate.opsForValue().set("ccc", "333");
        
        strRedis.opsForValue().set("zly-cache", "Hello Redis ~");
        
        Assert.assertEquals("111", redis.get("aaa"));
        Assert.assertEquals("222", strRedis.opsForValue().get("bbb"));
        Assert.assertEquals("333", redisTemplate.opsForValue().get("ccc"));
    }
    
    @Test
    @Ignore
	public void test(){	
		SysUser user = new SysUser();
		user.setId("test001");
		user.setUsername("test001_" + DateUtil.getNowTimeStamp());
		user.setNickname("test001_" + DateUtil.getNowTimeStamp());
		user.setPassword("test001_" + DateUtil.getNowTimeStamp());
		user.setAge(19);
		user.setJob(12);
		user.setIsDelete(0);
		user.setRegistTime(new Date());
		strRedis.opsForValue().set("json:user", JsonUtils.objectToJson(user));
		SysUser jsonUser =JsonUtils.jsonToPojo(strRedis.opsForValue().get("json:user"), SysUser.class);
		System.out.println(jsonUser);
	}
	
    @Test
	public void getJsonList(){
		SysUser user = new SysUser();
		user.setUsername("test001_" + DateUtil.getNowTimeStamp());
		user.setNickname("test001_" + DateUtil.getNowTimeStamp());
		user.setPassword("test001_" + DateUtil.getNowTimeStamp());
		user.setAge(19);
		user.setJob(12);
		user.setIsDelete(0);
		user.setRegistTime(new Date());
		
		SysUser user1 = new SysUser();
		user1.setUsername("test002_" + DateUtil.getNowTimeStamp());
		user1.setNickname("test002_" + DateUtil.getNowTimeStamp());
		user1.setPassword("test002_" + DateUtil.getNowTimeStamp());
		user1.setAge(21);
		user1.setJob(12);
		user1.setIsDelete(0);
		user1.setRegistTime(new Date());
		
		SysUser user2 = new SysUser();
		user2.setUsername("test003_" + DateUtil.getNowTimeStamp());
		user2.setNickname("test003_" + DateUtil.getNowTimeStamp());
		user2.setPassword("test003_" + DateUtil.getNowTimeStamp());
		user2.setAge(21);
		user2.setJob(12);
		user2.setIsDelete(0);
		user2.setRegistTime(new Date());
		
		List<SysUser> userList = new ArrayList<SysUser>();
		userList.add(user);
		userList.add(user1);
		userList.add(user2);
		
		redis.set("json:info:userList", JsonUtils.objectToJson(userList),2000);
		String userListJson= redis.get("json:info:userList");
		List<SysUser> userListResult = JsonUtils.jsonToList(userListJson, SysUser.class);
		System.out.println(userListResult);
	}
    
}