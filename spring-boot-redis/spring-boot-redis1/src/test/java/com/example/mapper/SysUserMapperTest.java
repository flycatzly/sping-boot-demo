package com.example.mapper;

import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.pojo.SysUser;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SysUserMapperTest {

	@Autowired
	private SysUserMapper userMapper;

	@Test
	@Ignore
	public void testInsert() throws Exception {
		SysUser sysUser= new SysUser();
		sysUser.setId("1111");
		sysUser.setUsername("ZLY");
		sysUser.setNickname("ZLY111");
		sysUser.setRegistTime(new Date());
		userMapper.insert(sysUser);
		Assert.assertEquals(10, userMapper.getAll().size());
	}

	@Test
	@Ignore
	public void testQuery() throws Exception {
		List<SysUser> users = userMapper.getAll();
		if(users==null || users.size()==0){
			System.out.println("is null");
		}else{
			System.out.println(users.toString());
		}
	}
	
	
	@Test
	@Ignore
	public void testUpdate() throws Exception {
		SysUser user = userMapper.getOne("111");
		System.out.println(user.toString());
		user.setNickname("zly");
		userMapper.update(user);
		Assert.assertTrue(("zly".equals(userMapper.getOne("111").getNickname())));
	}
	
	@Test
	@Ignore
	public void testDelete() throws Exception {
		SysUser user = userMapper.getOne("111");
		System.out.println(user.toString());
		userMapper.delete("111");
	}


}