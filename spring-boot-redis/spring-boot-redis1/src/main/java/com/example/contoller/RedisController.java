package com.example.contoller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.pojo.IZlyJSONResult;
import com.example.pojo.SysUser;
import com.example.utils.DateUtil;
import com.example.utils.JsonUtils;
import com.example.utils.RedisUtils;

@RestController
@RequestMapping("/redis")
public class RedisController {

	@Autowired
	private StringRedisTemplate strRedis;
	
	@Autowired
	private RedisUtils redis;

	@RequestMapping("/test")
	public IZlyJSONResult test(){
		redis.set("zly-cache", "Hello Redis ~");
		String result =redis.get("zly-cache");
		return IZlyJSONResult.ok(result);
	}
	
	@RequestMapping("/getObj")
	public IZlyJSONResult getObj(){

		SysUser user = new SysUser();
		user.setId("test001");
		user.setUsername("test001_" + DateUtil.getNowTimeStamp());
		user.setNickname("test001_" + DateUtil.getNowTimeStamp());
		user.setPassword("test001_" + DateUtil.getNowTimeStamp());
		user.setAge(19);
		user.setJob(12);
		user.setIsDelete(0);
		user.setRegistTime(new Date());
		strRedis.opsForValue().set("json:user", JsonUtils.objectToJson(user));
		SysUser jsonUser =JsonUtils.jsonToPojo(strRedis.opsForValue().get("json:user"), SysUser.class);
		return IZlyJSONResult.ok(jsonUser);
	}
	
	@RequestMapping("/getJsonList")
	public IZlyJSONResult getJsonList(){
		SysUser user = new SysUser();
		user.setUsername("test001_" + DateUtil.getNowTimeStamp());
		user.setNickname("test001_" + DateUtil.getNowTimeStamp());
		user.setPassword("test001_" + DateUtil.getNowTimeStamp());
		user.setAge(19);
		user.setJob(12);
		user.setIsDelete(0);
		user.setRegistTime(new Date());
		
		SysUser user1 = new SysUser();
		user1.setUsername("test002_" + DateUtil.getNowTimeStamp());
		user1.setNickname("test002_" + DateUtil.getNowTimeStamp());
		user1.setPassword("test002_" + DateUtil.getNowTimeStamp());
		user1.setAge(21);
		user1.setJob(12);
		user1.setIsDelete(0);
		user1.setRegistTime(new Date());
		
		SysUser user2 = new SysUser();
		user2.setUsername("test003_" + DateUtil.getNowTimeStamp());
		user2.setNickname("test003_" + DateUtil.getNowTimeStamp());
		user2.setPassword("test003_" + DateUtil.getNowTimeStamp());
		user2.setAge(21);
		user2.setJob(12);
		user2.setIsDelete(0);
		user2.setRegistTime(new Date());
		
		List<SysUser> userList = new ArrayList<SysUser>();
		userList.add(user);
		userList.add(user1);
		userList.add(user2);
		
		redis.set("json:info:userList", JsonUtils.objectToJson(userList),2000);
		String userListJson= redis.get("json:info:userList");
		List<SysUser> userListResult = JsonUtils.jsonToList(userListJson, SysUser.class);
		return IZlyJSONResult.ok(userListResult);
	}

}
