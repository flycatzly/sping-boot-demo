package com.example.contoller;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.thymeleaf.util.StringUtils;

import com.example.mapper.SysUserMapper;
import com.example.pojo.IZlyJSONResult;
import com.example.pojo.SysUser;
import com.example.utils.DateUtil;

@RestController
@RequestMapping("/sysUser")
public class SysUserController {

	@Autowired
	private SysUserMapper sysUserMapper;

    @RequestMapping("/uid")
    String uid(HttpSession session) {
        UUID uid = (UUID) session.getAttribute("uid");
        if (uid == null) {
            uid = UUID.randomUUID();
        }
        session.setAttribute("uid", uid);
        return session.getId();
    }
    
    @RequestMapping("/getUsers")
    @Cacheable(value="user-key")
    public SysUser getUser() throws Exception {
		SysUser user = new SysUser();
		String userId = DateUtil.getNowTimeStamp();
		user.setId(userId);
		user.setUsername("FlycatZly" + DateUtil.getNowTimeStamp());
		user.setNickname("FlycatZly" + DateUtil.getNowTimeStamp());
		user.setPassword("621014");
		user.setAge(18);
		user.setIsDelete(0);
		user.setRegistTime(new Date());
		//userService.saveUser(user);
        System.out.println("若下面没出现“无缓存的时候调用”字样且能打印出数据表示测试成功");
        return user;
    }
	
    @RequestMapping("/getUserList")
	public List<SysUser> getUsers() {
		List<SysUser> users=sysUserMapper.getAll();
		return users;
	}
	
    @RequestMapping("/getUser")
    public SysUser getUser(String id) {
    	SysUser user=sysUserMapper.getOne(id);
        return user;
    }
    
    @RequestMapping("/add")
    public void save(SysUser user) {
    	sysUserMapper.insert(user);
    }
    
    @RequestMapping(value="update")
    public void update(SysUser user) {
    	sysUserMapper.update(user);
    }
    
    @RequestMapping(value="/delete/{id}")
    public void delete(@PathVariable("id") String id) {
    	sysUserMapper.delete(id);
    }
	
}
