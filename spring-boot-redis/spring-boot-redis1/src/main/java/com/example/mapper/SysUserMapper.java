package com.example.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.example.enums.SysUserSexEnum;
import com.example.pojo.SysUser;

public interface SysUserMapper{
	
	@Select("SELECT * FROM sys_user")
	@Results({
		@Result(property = "sex",  column = "sex", javaType = SysUserSexEnum.class),
		@Result(property = "username", column = "username")
	})
	List<SysUser> getAll();
	
	@Select("SELECT * FROM sys_user WHERE id = #{id}")
	@Results({
		@Result(property = "sex",  column = "sex", javaType = SysUserSexEnum.class),
		@Result(property = "username", column = "username")
	})
	SysUser getOne(String id);

	@Insert("INSERT INTO sys_user(id, username, password, nickname, age, sex, job, face_image, province, city, district, address, auth_salt, last_login_ip, last_login_time, is_delete, regist_time) VALUES(#{id},#{username},#{password},#{nickname},#{age},#{sex},#{job},#{faceImage},#{province},#{city},#{district},#{address},#{authSalt},#{lastLoginIp},#{lastLoginTime},#{isDelete},#{registTime})")
	void insert(SysUser user);

	@Update("UPDATE sys_user SET username=#{username},nickname=#{nickname} WHERE id =#{id}")
	void update(SysUser user);

	@Delete("DELETE FROM sys_user WHERE id =#{id}")
	void delete(String id);
	
}