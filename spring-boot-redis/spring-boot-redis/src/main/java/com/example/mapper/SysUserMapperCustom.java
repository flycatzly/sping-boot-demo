package com.example.mapper;

import java.util.List;

import com.example.pojo.SysUser;

/**
 *  自定义mapper 
 * @author Administrator
 *
 */
public interface SysUserMapperCustom {

	List<SysUser> queryUserSimplyInfoById(String id);
}
