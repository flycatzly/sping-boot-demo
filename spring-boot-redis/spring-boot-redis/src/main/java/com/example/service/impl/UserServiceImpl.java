package com.example.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.thymeleaf.util.StringUtils;

import com.example.mapper.SysUserMapper;
import com.example.mapper.SysUserMapperCustom;
import com.example.pojo.SysUser;
import com.example.service.UserService;
import com.github.pagehelper.PageHelper;

import tk.mybatis.mapper.entity.Example;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private SysUserMapper userMapper;
	
	@Autowired
	private SysUserMapperCustom sysUserMapperCustom;
	
	@Override
	@Transactional(propagation =Propagation.REQUIRED)
	public void saveUser(SysUser user) {
		userMapper.insert(user);
	}

	@Override
	@Transactional(propagation =Propagation.REQUIRED)
	public void updateUser(SysUser user) {
		userMapper.updateByPrimaryKeySelective(user);
	}

	@Override
	@Transactional(propagation =Propagation.REQUIRED)
	public void deleteUser(String userId) {
		userMapper.deleteByPrimaryKey(userId);
	}

	@Override
	@Transactional(propagation =Propagation.SUPPORTS)
	public SysUser queryUserById(String userId) {
		return userMapper.selectByPrimaryKey(userId);
	}

	@Override
	@Transactional(propagation =Propagation.SUPPORTS)
	public List<SysUser> queryUserList(SysUser user) {
		Example example = new Example(SysUser.class);
		Example.Criteria criteria = example.createCriteria();
		if (!StringUtils.isEmptyOrWhitespace(user.getUsername())) {
			criteria.andLike("username", "%"+user.getUsername()+"%");
		}
		if (!StringUtils.isEmptyOrWhitespace(user.getNickname())) {
			criteria.andLike("nickname", "%" + user.getNickname() + "%");
		}
		List<SysUser> userList = userMapper.selectByExample(example);
		return userList; 
	}

	/**
	 * 分页查询
	 */
	@Override
	@Transactional(propagation =Propagation.SUPPORTS)
	public List<SysUser> queryUserListPaged(SysUser user, Integer pageNum, Integer pageSize) {
		// 开始分页
        PageHelper.startPage(pageNum, pageSize);
		
		Example example = new Example(SysUser.class);
		Example.Criteria criteria = example.createCriteria();
		
		if (!StringUtils.isEmptyOrWhitespace(user.getNickname())) {
			criteria.andLike("nickname", "%" + user.getNickname() + "%");
		}
		example.orderBy("registTime").desc();
		List<SysUser> userList = userMapper.selectByExample(example);
		if(userList!=null && !userList.isEmpty()){
			return userList;
		}
		return null;
	}

	/**
	 *  自定义mapper 查询
	 */
	@Override
	@Transactional(propagation =Propagation.SUPPORTS)
	public SysUser queryUserByIdCustom(String userId) {
		List<SysUser> userList = sysUserMapperCustom.queryUserSimplyInfoById(userId);
		if(userList!=null && !userList.isEmpty()){
			return userList.get(0);
		}
		return null;
	}

	/**
	 * 使用事物  查询用SUPPORTS  其他用REQUIRED
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void saveUserTransactional(SysUser user) {
		
		userMapper.insert(user);
		
		int a = 1 / 0;
		
		user.setIsDelete(1);
		userMapper.updateByPrimaryKeySelective(user);
	}

}
