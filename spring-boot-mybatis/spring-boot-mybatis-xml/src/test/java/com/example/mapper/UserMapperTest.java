package com.example.mapper;

import java.util.List;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.enums.UserSexEnum;
import com.example.model.User;
@RunWith(SpringRunner.class)
@SpringBootTest
public class UserMapperTest {

	@Autowired
	private UserMapper userMapper;
	
	@Test
	@Ignore
	public void testGetAll() {
		List<User> list =userMapper.getAll();
		System.out.println(list.toString());
		Assert.assertNotEquals(0, list.size());
	}

	@Test
	@Ignore
	public void testGetOne() {
		User user =userMapper.getOne(2L);
		System.out.println(user.toString());
	}

	@Test
	@Ignore
	public void testInsert() {
		User user = new User(com.example.utils.DateUtil.getNowTimeStamp()+"zly", com.example.utils.DateUtil.getNowTimeStamp()+"222", UserSexEnum.MAN);
		userMapper.insert(user);
		System.out.println(user.toString());
	}

	@Test
	@Ignore
	public void testUpdate() {
		User user = new User("zly1", "222", UserSexEnum.MAN);
		user.setId(2L);
		userMapper.update(user);
		System.out.println(user.toString());
	}

	@Test
	@Ignore
	public void testDelete() {
		userMapper.delete(33L);
		System.out.println(userMapper.getAll().size());
	}

}
