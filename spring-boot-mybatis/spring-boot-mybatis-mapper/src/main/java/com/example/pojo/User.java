package com.example.pojo;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;

@Data
public class User {
	private String name;
	private String password;
	@JsonIgnore /* 这个注解是不显示 */
	private Integer age;
	@JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss a", locale = "zh", timezone = "GMT+8")
	private Date birthday;
	@JsonInclude(Include.NON_NULL) /* 如果为空不显示 */
	private String desc;
}
