package com.example.pojo;

import java.util.Date;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;

@Data
@Table(name = "sys_user")
public class SysUser {
	@Id
	private String id;

	/**
	 * 用户名，登录名
	 */
	private String username;

	/**
	 * 密码
	 */
	@JsonInclude(Include.NON_NULL)
	private String password;

	/**
	 * 昵称
	 */
	private String nickname;

	/**
	 * 年龄
	 */
	@JsonInclude(Include.NON_NULL)
	private Integer age;

	/**
	 * 性别 0：女 1：男 2：保密
	 */
	@JsonInclude(Include.NON_NULL)
	private Integer sex;

	/**
	 * 职业类型： 1：Java开发 2：前端开发 3：大数据开发 4：ios开发 5：Android开发 6：Linux系统工程师 7：PHP开发
	 * 8：.net开发 9：C/C++ 10：学生 11：其它
	 */
	@JsonInclude(Include.NON_NULL)
	private Integer job;

	/**
	 * 头像地址
	 */
	@JsonInclude(Include.NON_NULL)
	@Column(name = "face_image")
	private String faceImage;

	/**
	 * 省
	 */
	@JsonInclude(Include.NON_NULL)
	private String province;

	/**
	 * 市
	 */
	@JsonInclude(Include.NON_NULL)
	private String city;

	/**
	 * 区
	 */
	@JsonInclude(Include.NON_NULL)
	private String district;

	/**
	 * 详细地址
	 */
	@JsonInclude(Include.NON_NULL)
	private String address;

	/**
	 * 用于权限的“盐”
	 */
	@JsonInclude(Include.NON_NULL)
	@Column(name = "auth_salt")
	private String authSalt;

	/**
	 * 最后一次登录IP
	 */
	@JsonInclude(Include.NON_NULL)
	@Column(name = "last_login_ip")
	private String lastLoginIp;

	/**
	 * 最后一次登录时间
	 */
	@JsonInclude(Include.NON_NULL)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
	@Column(name = "last_login_time")
	private Date lastLoginTime;

	@JsonInclude(Include.NON_NULL)
	@Column(name = "is_delete")
	private Integer isDelete;

	@JsonInclude(Include.NON_NULL)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
	@Column(name = "regist_time")
	private Date registTime;
}