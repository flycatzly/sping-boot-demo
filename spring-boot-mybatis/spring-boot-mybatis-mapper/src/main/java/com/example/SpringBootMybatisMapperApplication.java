package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import tk.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@MapperScan(basePackages="com.example.mapper")	//扫描mapper包路径
@ComponentScan(basePackages= {"com.example","org.n3r.idworker"})	//扫描所需要的包，包含自用的工具类包
public class SpringBootMybatisMapperApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootMybatisMapperApplication.class, args);
        String strConsole="\n" +
        		"    *           *      \n" +
    		    " *     *     *     *	\n" +
    		    " *        *        *   \n" +
    		    "  *   ZlyFlycat   *	\n" +
    		    "    *           *   	\n" +
    		    "       *     *      	\n" +
    		    "          *    		\n"+
    		    "  ლ(´ڡ`ლ)ﾞ  ZlyFlycatAdmin started successfully!!!  (♥◠‿◠)ﾉﾞ  \n";
        System.out.println(strConsole);
	}

}
