package com.example.repository;

import java.math.BigDecimal;

import org.assertj.core.util.DateUtil;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.entity.Luckymoney;

@RunWith(SpringRunner.class)
@SpringBootTest
public class LuckymoneyRepositoryTest {

	@Autowired
	private LuckymoneyRepository repository;
	
	@Test
	@Ignore
	public void testFindByProducerAndConsumer() {
		Luckymoney luckymoney =new Luckymoney();
		String producer="zly";
		String consumer="aa";
		luckymoney =repository.findByProducerAndConsumer(producer, consumer);
		System.out.println(luckymoney.toString());
	}

	@Test
	@Ignore
	public void testFindById() {
		Luckymoney luckymoney =new Luckymoney();
		Integer id =1;
		luckymoney =repository.findById(id).orElse(null);
		System.out.println(luckymoney.toString());
	}
	
	@Test
	@Ignore
	public void testSave() {
		Luckymoney luckymoney= new Luckymoney();
		luckymoney.setProducer("zly");
		luckymoney.setConsumer("consumer");
		luckymoney.setMoney(new BigDecimal(2.555));
		repository.save(luckymoney);
		System.out.println(luckymoney.toString());
	}
}
