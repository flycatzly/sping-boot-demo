package com.example.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.example.entity.Luckymoney;

//public interface LuckymoneyRepository extends JpaRepository<OrderDetail, String> {
public interface LuckymoneyRepository extends JpaRepository<Luckymoney, Integer>,JpaSpecificationExecutor<Luckymoney>,Serializable{

	Luckymoney findByProducerAndConsumer(String producer, String consumer);
    
//  Page<Luckymoney> findALL(Pageable pageable);
    
//	List<Luckymoney> findByUserNameIn(List<String> UserNameList);
//	
//	@Transactional
//	@Modifying
//	@Query("delete from Luckymoney where userName = ?1")
//	void deleteByUserName(String userName);
//	
//	@Transactional
//	@Modifying
//	@Query("delete from Luckymoney where id = ?1")
//	void deleteById(long id);
	
}
