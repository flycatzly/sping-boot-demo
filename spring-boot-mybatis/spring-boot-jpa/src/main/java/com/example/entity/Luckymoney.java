package com.example.entity;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.Data;

/**
 * 红包实体
 */
@Entity
@Data
public class Luckymoney {

	@Id
	@GeneratedValue
	private Integer id;
	
	private BigDecimal money;
	
	/**
	 * 发送方
	 */
	private String producer;
	
	/**
	 * 接收方
	 */
	private String consumer;

	public Luckymoney() {
	}

	@Override
	public String toString() {
		return "Luckmoney [id=" + id + ", money=" + money + ", producer=" + producer + ", consumer=" + consumer + "]";
	}
	
}
