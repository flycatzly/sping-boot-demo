package com.example.service;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.entity.Luckymoney;
import com.example.repository.LuckymoneyRepository;

@Service
public class LunckymoneyService {
	
	@Autowired
	private LuckymoneyRepository repository;
	
	@Transactional
	public void createTwo() {
		Luckymoney luckymoney= new Luckymoney();
		luckymoney.setProducer("zzz");
		luckymoney.setMoney(new BigDecimal("11.101"));
		repository.save(luckymoney);
		
		Luckymoney luckymoney2= new Luckymoney();
		luckymoney.setProducer("zz1");
		luckymoney.setMoney(new BigDecimal("33.101"));
		repository.save(luckymoney2);
	}
}
