package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZlyApplication {

	public static void main(String[] args) {
		SpringApplication.run(ZlyApplication.class, args);
        String strConsole="\n" +
        		"    *           *      \n" +
    		    " *     *     *     *	\n" +
    		    " *        *        *   \n" +
    		    "  *   ZlyFlycat   *	\n" +
    		    "    *           *   	\n" +
    		    "       *     *      	\n" +
    		    "          *    		\n"+
    		    " ლ(´ڡ`ლ)ﾞ  ZlyFlycatAdmin started successfully!!!  (♥◠‿◠)ﾉﾞ  \n";
        System.out.println(strConsole);
	}
}
