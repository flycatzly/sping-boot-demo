package com.zly.model.repository;

import com.zly.model.repository.primary.PrimaryMongoObject;
import com.zly.model.repository.primary.PrimaryRepository;
import com.zly.model.repository.secondary.SecondaryMongoObject;
import com.zly.model.repository.secondary.SecondaryRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

/**
 * Created by zly on 2017/5/6.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class MuliDatabaseTest {

    @Autowired
    private PrimaryRepository primaryRepository;

    @Autowired
    private SecondaryRepository secondaryRepository;

    @Test
    public void TestSave() {

        System.out.println("****************************START********************************");
        PrimaryMongoObject primaryObj = new PrimaryMongoObject();
        primaryObj.setId(null);
        primaryObj.setValue("第一个库的对象");
        this.primaryRepository.save(primaryObj);
        
        SecondaryMongoObject secondaryObj = new SecondaryMongoObject();
        secondaryObj.setId(null);
        secondaryObj.setValue("第二个库的对象");
        this.secondaryRepository.save(secondaryObj);

        List<PrimaryMongoObject> primaries = this.primaryRepository.findAll();
        for (PrimaryMongoObject primary : primaries) {
            System.out.println(primary.toString());
        }

        List<SecondaryMongoObject> secondaries = this.secondaryRepository.findAll();

        for (SecondaryMongoObject secondary : secondaries) {
            System.out.println(secondary.toString());
        }

        System.out.println("************************************************************");
        System.out.println("测试完成");
        System.out.println("*****************************END*******************************");
    }

    @Test
    public void TestSave1() {

        System.out.println("****************************START********************************");
        PrimaryMongoObject primaryObj = new PrimaryMongoObject();
        primaryObj.setId(null);
        primaryObj.setValue("第一个库的对象");
        this.primaryRepository.save(primaryObj);
        System.out.println("******************END*******************"+primaryObj);
    }
}
