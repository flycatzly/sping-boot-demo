package com.zly.dao;

import com.zly.entity.UserEntity;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserDaoTest {

    @Autowired
    private UserDao userDao;

    @Test
    public void testSaveUser() throws Exception {
        UserEntity user=new UserEntity();
        user.setId(1l);
        user.setUserName("小明");
        user.setPassWord("fffooo123");
        userDao.saveUser(user);
        System.out.println("success");
    }

    @Test
    public void findUserByUserName(){
       UserEntity user= userDao.findUserByUserName("小明");
       System.out.println("user is "+user);
    }

    @Test
    public void updateUser(){
        UserEntity user=new UserEntity();
        user.setId(1l);
        user.setUserName("天空");
        user.setPassWord("fffxxxx");
        boolean flag =userDao.updateUser(user);
        System.out.println("修改："+flag);
    }

    @Test
    public void deleteUserById(){
        userDao.deleteUserById(1l);
    }

}
