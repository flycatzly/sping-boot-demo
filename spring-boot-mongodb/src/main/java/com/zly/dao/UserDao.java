package com.zly.dao;

import com.zly.entity.UserEntity;

public interface UserDao  {

    public void saveUser(UserEntity user);

    public UserEntity findUserByUserName(String userName);

    public boolean updateUser(UserEntity user);

    public void deleteUserById(Long id);

}
